// @flow
import React from 'react';
import ReactDOM from 'react-dom';
import App from './view/app/App';
import rootSaga from './rootSaga';
import StoreFactory, {HistoryFactory} from './store/store';
import {unregister} from './registerServiceWorker';
import rootReducer from './rootReducer';
import './view/icons';
import './index.css';



const history = HistoryFactory.createHistory('/hotel');
const store = StoreFactory.createStore(rootReducer, rootSaga, history);
if (process.env.NODE_ENV === 'development') {
  console.info('%cAttaching store to global scope. You can access it in the console using the variable %cstore%c.',
    'color: gray; font-weight: lighter;', 'font-weight: bold;', 'font-weight: inherit;');
  global.store = store;
}
unregister();
// $FlowFixMe: there will always be a document
ReactDOM.render(<App store={store} history={history}/>, document.getElementById('root'));
