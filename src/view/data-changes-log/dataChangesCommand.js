//@flow

export default class DataChangesCommand {
  static get GET_CHANGES(): string {
    return 'SponsorsCommand/GET_CHANGES';
  }
}

export const getChanges = (fromDate: string) => ({type: DataChangesCommand.GET_CHANGES, fromDate});
