// @flow

import DataChangesEvents from './dataChangesEvents';
export type DataChangeValue = {content: Object, type: string}
export type DataChange = {
  type: string,
  date: string,
  oldValue: DataChangeValue,
  newValue: DataChangeValue
}

export type SponsorsState = {
  changes: DataChange[],
}

export const INITIAL_STATE: SponsorsState = {
  changes: []
};

export const dataChangesReducer = (state: SponsorsState = INITIAL_STATE, action: any) => {
  switch (action.type) {
    case DataChangesEvents.GET_CHANGES_SUCCEEDED:
      return {...state, changes: action.changes};
    case DataChangesEvents.GET_CHANGES_FAILED:
      return {...state, changes: []};
    default:
      return state;
  }
};

export default {dataChanges: dataChangesReducer};
