// @flow


import {getChangesFailed, getChangesSucceeded} from './dataChangesEvents';
import {dataChangesReducer, INITIAL_STATE} from './dataChangesReducer';
import type {DataChange} from './dataChangesReducer';

describe('Data changes reducer', () => {
  it('stores changes when GET_CHANGES_SUCCEEDED', () => {
    const changes: DataChange[] = [{
      type: 'type',
      date: '2000-01-01T00:00:00.000Z',
      oldValue: {content: {data: 'old value'}, type: 'type'},
      newValue: {content: {data: 'new value'}, type: 'type'}
    }];
    const event = getChangesSucceeded(changes);
    const result = dataChangesReducer(INITIAL_STATE, event);
    expect(result.changes).toBeDefined();
    expect(result.changes).toHaveLength(1);
  });
  it('stores empty arrays hints when GET_CHANGES_FAILED', () => {
    const event = getChangesFailed(new Error('Error'));
    const result = dataChangesReducer(INITIAL_STATE, event);
    expect(result.changes).toBeDefined();
    expect(result.changes).toHaveLength(0);
  });
  it('lets store untouched, when unknown event type', () => {
    const event = {type: 'unknown', payload: {}};
    const result = dataChangesReducer(INITIAL_STATE, event);
    expect(result).toEqual(INITIAL_STATE);
  });
});
