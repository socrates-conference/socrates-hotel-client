// @flow
import axios from 'axios';
import SagaTester from 'redux-saga-tester';
import rootSaga from '../../rootSaga';
import {dataChangesReducer, INITIAL_STATE} from './dataChangesReducer';
import {getChanges} from './dataChangesCommand';
import DataChangesEvents from './dataChangesEvents';

jest.mock('axios');

const changes = [
  {type: 'type', oldValue: 'old value', newValue: 'new value'}
];
describe('on data changes saga', () => {
  let sagaTester: SagaTester;
  let promise;
  beforeEach(() => {
    sagaTester = new SagaTester({INITIAL_STATE, reducers: dataChangesReducer});
    sagaTester.start(rootSaga);
  });

  afterEach(() => {
    sagaTester.reset();
  });

  describe('getting room types', () => {
    beforeEach(() => {
      axios.get.mockImplementation(() => promise);
    });

    afterEach(() => {
      axios.get.mockReset();
    });

    it('dispatches GET_ROOM_TYPES_SUCCEEDED', async () => {
      promise = Promise.resolve({data: changes});
      sagaTester.dispatch(getChanges('01.01.2000 00:00:00'));
      await sagaTester.waitFor(DataChangesEvents.GET_CHANGES_SUCCEEDED);
    });
    it('catches errors and dispatches GET_ROOM_TYPES_SUCCEEDED', async () => {
      promise = Promise.reject({message: 'error'});
      sagaTester.dispatch(getChanges('01.01.2000 00:00:00'));
      await sagaTester.waitFor(DataChangesEvents.GET_CHANGES_SUCCEEDED);
    });
  });
});
