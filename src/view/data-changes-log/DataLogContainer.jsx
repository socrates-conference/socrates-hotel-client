// @flow

import React from 'react';
import {connect} from 'react-redux';
import * as PropTypes from 'prop-types';
import {ColumnDefinition, Table} from 'socrates-ui-components';
import type {DataChange, DataChangeValue} from './dataChangesReducer';
import {getChanges} from './dataChangesCommand';

type Props = {
  getChanges: (fromDate: string) => void,
  changes: DataChange[],
}


export class DataLogContainer extends React.Component<Props> {

  static propTypes = {
    changes: PropTypes.array.isRequired,
    getChanges: PropTypes.func.isRequired
  };

  _columns: ColumnDefinition[] = [
    {header: 'Date', field: 'date', visible: true},
    {header: 'Type', field: 'type', visible: true, cellTemplate: (type) => this._typeCellTemplate(type)},
    {
      header: 'Old value', field: 'oldValue', visible: true,
      cellTemplate: (oldValue) => this._valueCellTemplate(oldValue)
    },
    {
      header: 'New Value', field: 'newValue', visible: true,
      cellTemplate: (newValue) => this._valueCellTemplate(newValue)
    }
  ];

  _valueCellTemplate = (value: DataChangeValue) => {
    switch (value.type) {
      case 'LOG_PARTICIPANT_DELETED':
      case 'LOG_PARTICIPANT_CANCELED':
      case 'LOG_PARTICIPANT_CONFIRMED':
        return this._nameAndAddressCellTemplate(value.content);
      case 'LOG_UPDATE_ROOM_TYPE_SUCCEED':
        return this._roomTypeChangedCellTemplate(value.content);
      case 'LOG_PARTICIPANT_UPDATED':
        return this._profileCellTemplate(value.content);
      default:
        return <div>unknown change type</div>;
    }
  };
  _typeCellTemplate = (type: Object) => {
    switch (type) {
      case 'LOG_PARTICIPANT_DELETED':
      case 'LOG_PARTICIPANT_CANCELED':
        return 'Cancellation';
      case 'LOG_PARTICIPANT_CONFIRMED':
        return 'New Participant';
      case 'LOG_UPDATE_ROOM_TYPE_SUCCEED':
        return 'Room type changed';
      case 'LOG_PARTICIPANT_UPDATED':
        return 'Participant data changed';
      default:
        return 'unknown change type';
    }
  };

  _roomTypeChangedCellTemplate = (value: Object) =>
    <div>
      <div>
        {value.firstname} {value.lastname}
      </div>
      <div>
        {value.roomType}
      </div>
    </div>;

  _nameAndAddressCellTemplate = (value: Object) =>
    <div>
      <div>
        {value.firstname} {value.lastname}
      </div>
      {value.additional && <div>
        {value.additional}
      </div>}
      {value.company && <div>
        {value.company}
      </div>}
      <div>
        {value.address1}
      </div>
      {value.address2 && <div>
        {value.address2}
      </div>}
      {value.province && <div>
        {value.province}
      </div>}
      <div>
        {value.postal}&nbsp;{value.city}
      </div>
      <div>
        {value.country}
      </div>
    </div>;

  _profileCellTemplate = (value: Object) =>
    <div>
      <div>
        {value.firstname} {value.lastname}
      </div>
      {value.company && <div>
        {value.company}
      </div>}
      {value.address1 && <div>
        {value.address1}
      </div>}
      {value.address2 && <div>
        {value.address2}
      </div>}
      {value.province && <div>
        {value.province}
      </div>}
      {(value.postal || value.city) && <div>
        {value.postal}&nbsp;{value.city}
      </div>}
      {value.arrival && <div>
        Arrival: {value.arrival}
      </div>}
      {value.departure && <div>
        Departure: {value.departure}
      </div>}
      {value.dietary && <div>
        Dietary needs: {value.dietary ? 'Yes' : 'No'}
      </div>}
      {value.dietary && value.dietaryInfo && <div>
        {value.dietaryInfo}
      </div>}
      {value.family && <div>
        Family info: {value.family ? 'Yes' : 'No'}
      </div>}
      {value.family && value.familyInfo && <div>
        {value.familyInfo}
      </div>}
    </div>;

  componentDidMount(): void {
    this.refresh();
  }

  refresh(): void {
    this.props.getChanges('2000-01-01 00:00:00');
  }

  render = () => {
    return (
      <div className="container">
        <div className="row">
          <div className="col-sm-12">
            <div className="page-header">
              <h1>Data changes by date</h1>
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col-sm-12">
            <Table
              columns={this._columns}
              data={this.props.changes}/>
          </div>
        </div>
      </div>
    );
  };
}

const mapStateToProps = (state) => {
  return {...state.dataChanges};
};
const mapDispatchToProps = {
  getChanges
};

export default connect(mapStateToProps, mapDispatchToProps)(DataLogContainer);


