// @flow

import React from 'react';
import {shallow} from 'enzyme/build';
import {DataLogContainer} from './DataLogContainer';

const changes = [
  {
    type: 'LOG_PARTICIPANT_DELETED',
    oldValue: {
      'id': 243,
      'person_id': 7,
      'firstname': 'Alexandre',
      'lastname': 'Soler Sanandres',
      'company': '',
      'address1': 'Am Hirschanger 1',
      'address2': '',
      'province': '',
      'postal': '90610',
      'city': 'Winkelhaid',
      'country': 'Deutschland',
      'arrival': '0',
      'departure': '5',
      'dietary': false,
      'dietary_info': '',
      'family': false,
      'family_info': '',
      'gender': '',
      'tshirt': '12',
      'labelname': 'Alex',
      'social': '@xelamrelos',
      'pronoun': 'he',
      'personId': 7,
      'familyInfo': '',
      'dietaryInfo': ''
    },
    newValue: {}
  },
  {
    type: 'LOG_UPDATE_ROOM_TYPE_SUCCEED',
    oldValue: {
      'name': 'Alexandre Soler Sanandres',
      'email': 'alexandre@soler-sanandres.net',
      'date': '2019-04-15T19:09:25.000Z',
      'confirmationReminderCounter': 0,
      'roomType': 'juniorShared',
      'confirmationLastReminder': null,
      'firstname': 'Alex',
      'lastname': 'Soler Sanandres'
    },
    newValue: {
      'name': 'Alexandre Soler Sanandres',
      'email': 'alexandre@soler-sanandres.net',
      'date': '2019-04-15T19:09:25.000Z',
      'confirmationReminderCounter': 0,
      'roomType': 'bedInDouble',
      'confirmationLastReminder': null,
      'firstname': 'Alex',
      'lastname': 'Soler Sanandres'
    }
  },
  {
    type: 'LOG_PARTICIPANT_UPDATED',
    oldValue: {
      'firstname': 'Alex',
      'lastname': 'Soler Sanandres',
      'company': '',
      'address1': 'Am Hirschanger 1',
      'address2': '',
      'province': '',
      'postal': '90610',
      'city': 'Winkelhaid',
      'country': 'Deutschland',
      'arrival': 'Thursday evening',
      'departure': 'Sunday evening',
      'dietary': false,
      'dietaryInfo': '',
      'family': false,
      'familyInfo': ''
    },
    newValue: {
      'firstname': 'Alex',
      'lastname': 'Soler Sanandres',
      'company': '1971',
      'address1': 'Goethering, 54, 54, 54, 54',
      'address2': '54',
      'province': 'Franken',
      'postal': '90547',
      'city': 'Stein',
      'country': 'Deutschland',
      'arrival': 'Thursday afternoon',
      'departure': 'Monday',
      'dietary': true,
      'dietaryInfo': 'dietary needs',
      'family': true,
      'familyInfo': 'family info'
    }
  }
];
describe('(Component) DataLogContainer', () => {

  const wrapper = shallow(
    <DataLogContainer getChanges={() => {
    }} changes={[]}/>);

  it('renders without exploding', () => {
    expect(wrapper).toHaveLength(1);
  });

  it('renders correctly', () => {
    expect(wrapper).toMatchSnapshot();
  });
});
