// @flow

import type {DataChange} from './dataChangesReducer';

export default class DataChangesEvents {
  static get GET_CHANGES_SUCCEEDED() { return 'DataChangesEvents/GET_CHANGES_SUCCEEDED'; }
  static get GET_CHANGES_FAILED() { return 'DataChangesEvents/GET_CHANGES_FAILED'; }
}

export const getChangesSucceeded =
  (changes: DataChange[]) => ({type: DataChangesEvents.GET_CHANGES_SUCCEEDED, changes});
export const getChangesFailed = (error: any) => ({type: DataChangesEvents.GET_CHANGES_FAILED, error});
