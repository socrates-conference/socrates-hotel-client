// @flow
import * as api from '../../requests/api';
import {call, put} from 'redux-saga/effects';
import {getChangesSucceeded} from './dataChangesEvents';
import type {DataChange} from './dataChangesReducer';
import * as moment from 'moment';

export function* getChangesSaga(action: Object): Iterable<any> {
  let changes: DataChange[] = [];
  try {
    const rawChanges = yield call(api.readChanges, action.fromDate);
    if (rawChanges) {
      changes = rawChanges.map((item) => ({
        type: item.type,
        date: moment(item.date).format('DD-MM-YYYY'),
        oldValue: {content: JSON.parse(item.oldValue), type: item.type},
        newValue: {content: JSON.parse(item.newValue), type: item.type}
      }));
    }
  } catch (e) {
    console.error('cannot read changes: ', e);
  }
  yield put(getChangesSucceeded(changes));
}
