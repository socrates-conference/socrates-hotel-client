// @flow

import SponsorsEvents from './sponsorsEvents';

export type Sponsor = {
  sponsorId: number,
  name: string,
  nameAdditional: ?string,
  address1: string,
  address2: ?string,
  province: ?string,
  postal: string,
  city: string,
  country: string,
  amount: number
}
export type Address = {
  address1: string,
  address2?: string,
  province?: string,
  postal: string,
  city: string,
  country: string
};

export type Name = { main: string, additional?: string };

export type SponsorBillingInformation = {
  sponsorId: number,
  name: Name,
  address: Address,
  amount: number
}

export type SponsorsState = {
  sponsors: SponsorBillingInformation[],
  totalSponsored: number
}

export const INITIAL_STATE: SponsorsState = {
  sponsors: [],
  totalSponsored: 0
};

export const sponsorsReducer = (state: SponsorsState = INITIAL_STATE, action: any) => {
  switch (action.type) {
    case SponsorsEvents.GET_TOTAL_SPONSORED_SUCCEEDED:
      return {...state, totalSponsored: action.amount};
    case SponsorsEvents.GET_SPONSORS_SUCCEEDED:
      return {...state, sponsors: action.sponsors};
    case SponsorsEvents.GET_SPONSORS_FAILED:
      return {...state, sponsors: []};
    default:
      return state;
  }
};

export default {sponsors: sponsorsReducer};
