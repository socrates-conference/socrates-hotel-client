// @flow
import * as api from '../../requests/api';
import {call, put} from 'redux-saga/effects';
import type {Sponsor, SponsorBillingInformation} from './sponsorsReducer';
import {getSponsorsFailed, getSponsorsSucceeded, getTotalSponsoredSucceeded} from './sponsorsEvents';

export function* getSponsorsSaga(): Iterable<any> {
  let sponsors: SponsorBillingInformation[] = [];
  try {
    const rawSponsors = yield call(api.readSponsors);
    if (rawSponsors) {
      sponsors = rawSponsors.map((item: Sponsor) => ({
        sponsorId: item.sponsorId,
        name: {main: item.name, additional: item.nameAdditional},
        address: {
          address1: item.address1, address2: item.address2, province: item.province,
          postal: item.postal, city: item.city, country: item.country
        },
        amount: item.amount
      }));
    }
  } catch (e) {
    console.error('cannot read sponsors: ', e);
  }
  if (sponsors) {
    yield put(getSponsorsSucceeded(sponsors));
  } else {
    yield put(getSponsorsFailed(new Error('cannot read sponsors.')));
  }
}
export function* getTotalSponsoredSaga(): Iterable<any> {
  const amount = yield call(api.readTotalSponsored);
  yield put(getTotalSponsoredSucceeded(amount ? amount : 0));
}
