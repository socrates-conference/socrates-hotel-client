//@flow

export default class SponsorsCommand {
  static get GET_SPONSORS(): string {
    return 'SponsorsCommand/GET_SPONSORS';
  }
  static get GET_TOTAL_SPONSORED(): string {
    return 'SponsorsCommand/GET_TOTAL_SPONSORED';
  }
}

export const getSponsors = () => ({type: SponsorsCommand.GET_SPONSORS});
export const getTotalSponsored = () => ({type: SponsorsCommand.GET_TOTAL_SPONSORED});
