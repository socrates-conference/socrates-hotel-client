// @flow

import type {SponsorBillingInformation} from './sponsorsReducer';

export default class SponsorsEvents {
  static get GET_SPONSORS_SUCCEEDED() { return 'SponsorsEvents/GET_SPONSORS_SUCCEEDED'; }
  static get GET_SPONSORS_FAILED() { return 'SponsorsEvents/GET_SPONSORS_FAILED'; }
  static get GET_TOTAL_SPONSORED_SUCCEEDED() { return 'SponsorsEvents/GET_TOTAL_SPONSORED_SUCCEEDED'; }
}

export const getSponsorsSucceeded =
  (sponsors: SponsorBillingInformation[]) => ({type: SponsorsEvents.GET_SPONSORS_SUCCEEDED, sponsors});
export const getSponsorsFailed = (error: any) => ({type: SponsorsEvents.GET_SPONSORS_FAILED, error});
export const getTotalSponsoredSucceeded =
  (amount: number) => ({type: SponsorsEvents.GET_TOTAL_SPONSORED_SUCCEEDED, amount});
