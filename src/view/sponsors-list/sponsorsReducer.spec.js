// @flow

import {INITIAL_STATE, sponsorsReducer} from './sponsorsReducer';
import {getSponsorsFailed, getSponsorsSucceeded} from './sponsorsEvents';

describe('Sponsors reducer', () => {
  it('stores sponsors when GET_SPONSORS_SUCCEEDED', () => {
    const event = getSponsorsSucceeded([{sponsorId: 4711, name: {main: 'main'}, address: {address1: 'address1',
      postal: '99999', city: 'city', country: 'country'}, amount: 100}]);
    const result = sponsorsReducer(INITIAL_STATE, event);
    expect(result.sponsors).toBeDefined();
  });
  it('stores empty arrays hints when GET_SPONSORS_FAILED', () => {
    const event = getSponsorsFailed(new Error('Error'));
    const result = sponsorsReducer(INITIAL_STATE, event);
    expect(result.sponsors).toBeDefined();
    expect(result.sponsors).toHaveLength(0);
  });
});
