// @flow

import React from 'react';
import {shallow} from 'enzyme/build';
import {Sponsors} from './Sponsors';

describe('(Component) Home', () => {

  const sponsors = [{sponsorId: 4711, name: {main: 'main'}, address: {address1: 'address1',
    postal: '99999', city: 'city', country: 'country'}, amount: 100}];
  const totalSponsored = 100;

  const wrapper = shallow(
    <Sponsors
      getSponsors={() => {}} sponsors={sponsors} getTotalSponsored={() => {}}
      totalSponsored={totalSponsored}
    />);

  it('renders without exploding', () => {
    expect(wrapper).toHaveLength(1);
  });

  it('renders correctly', () => {
    expect(wrapper).toMatchSnapshot();
  });
});
