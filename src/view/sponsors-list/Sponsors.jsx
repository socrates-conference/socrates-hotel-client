// @flow

import React from 'react';
import {connect} from 'react-redux';
import * as PropTypes from 'prop-types';
import {ColumnDefinition, Table} from 'socrates-ui-components';
import type {Address, Name, SponsorBillingInformation} from './sponsorsReducer';
import {getSponsors, getTotalSponsored} from './sponsorsCommand';

type Props = {
  getSponsors: () => void,
  getTotalSponsored: () => void,
  sponsors: SponsorBillingInformation[],
  totalSponsored: number
}

export class Sponsors extends React.Component<Props> {

  static propTypes = {
    getSponsors: PropTypes.func.isRequired,
    getTotalSponsored: PropTypes.func.isRequired,
    sponsors: PropTypes.array.isRequired
  };
  _sponsorsColumns: ColumnDefinition[] = [
    {header: 'Name', field: 'name', visible: true, cellTemplate: (name) => this._nameCellTemplate(name)},
    {header: 'Address', field: 'address', visible: true, cellTemplate: (address) => this._addressCellTemplate(address)},
    {header: 'Amount', field: 'amount', visible: true, format: (value: number) => `${value.toLocaleString()} €`}
  ];

  _nameCellTemplate = (name: Name) =>
    <div>
      <div>
        {name.main}
      </div>
      {name.additional && <div>
        {name.additional}
      </div>}
    </div>;

  _addressCellTemplate = (address: Address) =>
    <div>
      <div>
        {address.address1}
      </div>
      {address.address2 && <div>
        {address.address2}
      </div>}
      {address.province && <div>
        {address.province}
      </div>}
      <div>
        {address.postal}&nbsp;{address.city}
      </div>
      <div>
        {address.country}
      </div>
    </div>;


  componentDidMount(): void {
    this.refresh();
  }

  refresh(): void {
    this.props.getTotalSponsored();
    this.props.getSponsors();
  }

  render = () => {
    return (
      <div className="container">
        <div className="row">
          <div className="col-sm-12">
            <div className="page-header">
              <h1>Sponsors</h1>
            </div>
          </div>
        </div>
        <div>
          <h5 className="text-center"><b>Total sponsored: {this.props.totalSponsored.toLocaleString()}</b></h5>
        </div>
        <div className="row">
          <div className="col-sm-12">
            <Table
              columns={this._sponsorsColumns}
              data={this.props.sponsors}/>
          </div>
        </div>
      </div>
    );
  };
}

const mapStateToProps = (state) => {
  return {...state.sponsors};
};
const mapDispatchToProps = {
  getSponsors, getTotalSponsored
};

export default connect(mapStateToProps, mapDispatchToProps)(Sponsors);


