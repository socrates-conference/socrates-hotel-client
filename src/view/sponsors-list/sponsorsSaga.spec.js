// @flow
import axios from 'axios';
import SagaTester from 'redux-saga-tester';
import rootSaga from '../../rootSaga';
import {INITIAL_STATE, sponsorsReducer} from './sponsorsReducer';
import SponsorsEvents from './sponsorsEvents';
import {getSponsors, getTotalSponsored} from './sponsorsCommand';

jest.mock('axios');

describe('Sponsors saga', () => {
  let sagaTester: SagaTester;
  let promise;
  beforeEach(() => {
    sagaTester = new SagaTester({INITIAL_STATE, reducers: sponsorsReducer});
    sagaTester.start(rootSaga);
  });

  afterEach(() => {
    sagaTester.reset();
  });

  describe('get sponsors', () => {
    beforeEach(() => {
      axios.get.mockImplementation(() => promise);
    });

    afterEach(() => {
      axios.get.mockReset();
    });

    it('dispatches GET_SPONSORS_SUCCEEDED', async () => {
      promise = Promise.resolve({data: [{}]});
      sagaTester.dispatch(getSponsors());
      await sagaTester.waitFor(SponsorsEvents.GET_SPONSORS_SUCCEEDED);
    });
  });
  describe('get total sponsored', () => {
    beforeEach(() => {
      axios.get.mockImplementation(() => promise);
    });

    afterEach(() => {
      axios.get.mockReset();
    });

    it('dispatches GET_TOTAL_SPONSORED_SUCCEEDED', async () => {
      promise = Promise.resolve({data: 1000});
      sagaTester.dispatch(getTotalSponsored());
      await sagaTester.waitFor(SponsorsEvents.GET_TOTAL_SPONSORED_SUCCEEDED);
    });
  });

});
