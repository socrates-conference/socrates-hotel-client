import React from 'react';
import {ScrollToTop} from './ScrollToTop';
import {mount} from 'enzyme';

describe('Dashboard reducer', () => {
  const wrapper = mount(
    <ScrollToTop location={{hash: '#test'}}><div id="test">content</div></ScrollToTop>
  );

  it('renders without exploding', () => {
    expect(wrapper).toHaveLength(1);
  });

  it('renders correctly', () => {
    expect(wrapper).toMatchSnapshot();
  });
  it('renders correctly after props update', () => {
    wrapper.instance().componentDidUpdate({location:{}, children: []});
    wrapper.update();
    expect(wrapper).toMatchSnapshot();
  });
});
