// @flow
import {Link} from 'react-router-dom';
import React from 'react';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';

type DropdownItemProps = {
  visible: boolean,
  url: string,
  title: string,
  icon: string,
  onClick: () => void
}

export default function DropdownItem(props: DropdownItemProps) {
  DropdownItem.defaultProps = {
    onClick: () => {
    }
  };
  const linkClass = 'nav-link' + (props.visible ? ' d-block' : ' d-none');
  return (
    <Link
      className={linkClass}
      to={props.url}
      title={props.title}
      data-toggle="collapse"
      data-target=".navbar-collapse.show"
      onClick={props.onClick}
    >
      <FontAwesomeIcon icon={props.icon} />
      <span> {props.title}</span>
    </Link>
  );
}

