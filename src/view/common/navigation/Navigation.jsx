// @flow
import React from 'react';
import {connect} from 'react-redux';
import NavigationBrand from './NavigationBrand';
import NavigationLink from './NavigationLink';
import NavigationToggler from './NavigationToggler';
import DropdownItem from './DropdownItem';
import DropdownToggler from './DropdownToggler';
import {withRouter} from 'react-router';
import type {AuthenticationState} from '../../authentication/authenticationReducer';
import {logout} from '../../authentication/authenticationCommand';

import './styles.css';

type Props = {
  authentication: AuthenticationState,
  logout: () => void
}

// eslint-disable-next-line no-unused-vars
export function Navigation(props: Props) {
  const isLoggedIn = props.authentication.token.trim() !== '';
  const collapsiblePanelId = 'navbarSupportedContent';
  const userDropdown = 'navbarDropdownMenuLink';
  return (
    <div id="navigation">
      <nav className="navbar navbar-expand-md navbar-dark bg-dark">
        <NavigationBrand/>
        <NavigationToggler target={collapsiblePanelId}/>
        <div className="collapse navbar-collapse" id={collapsiblePanelId}>
          <ul className="navbar-nav mr-auto">
            <NavigationLink url="/sponsors" title="Sponsors" icon="donate"/>
            <NavigationLink url="/room-occupancy" title="Occupancy" icon="door-open"/>
            <NavigationLink url="/room-sharing" title="Room Sharing" icon="handshake"/>
            <NavigationLink url="/data-log" title="Changes" icon="clipboard-list"/>
            <NavigationLink url="/imprint" title="Imprint" icon="paragraph"/>
            <NavigationLink url="/privacy-policy" title="Privacy policy" icon="user-secret"/>
          </ul>
          <div className="navbar-nav navbar-item mr-3 dropdown">
            <DropdownToggler
              target={userDropdown}
              url="#navigation"
              title={props.authentication.userName}
              icon="user"
            />
            <div className="dropdown-menu bg-dark dropdown-menu-right" aria-labelledby={userDropdown}>
              <DropdownItem visible={!isLoggedIn} url="/login" title="Login" icon="sign-in-alt"/>
              <DropdownItem
                visible={isLoggedIn} url="#navigation" title="Logout" icon="sign-out-alt"
                onClick={props.logout}/>
            </div>
          </div>
        </div>
      </nav>
    </div>
  );
}

const mapStateToProps = (state) => {
  return {authentication: {...state.authentication}};
};

const mapDispatchToProps = {
  logout
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Navigation));
