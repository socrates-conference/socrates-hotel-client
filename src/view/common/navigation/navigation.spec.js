import React from 'react';
import {shallow} from 'enzyme';
import {Navigation} from './Navigation';

const authenticated = {
  token: 'theToken',
  isAdministrator: false,
  userName: 'Test',
  hasFinished: true
};
const notAuthenticated = {
  token: '',
  isAdministrator: false,
  userName: 'Guest',
  hasFinished: false
};

describe('(Component) Navigation', () => {
  describe('with logged in user', () => {
    const wrapper = shallow(<Navigation authentication={authenticated} logout={() => {}}/>);

    it('renders without exploding', () => {
      expect(wrapper).toHaveLength(1);
    });

    it('renders correctly', () => {
      expect(wrapper).toMatchSnapshot();
    });
  });
  describe('without logged in user', () => {
    const wrapper = shallow(<Navigation authentication={notAuthenticated} logout={() => {}}/>);

    it('renders without exploding', () => {
      expect(wrapper).toHaveLength(1);
    });

    it('renders correctly', () => {
      expect(wrapper).toMatchSnapshot();
    });
  });
});
