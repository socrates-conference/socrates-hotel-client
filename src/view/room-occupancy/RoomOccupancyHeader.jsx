// @flow

import React from 'react';
import * as PropTypes from 'prop-types';

type Props = {
  title: string
}
export default function RoomOccupancyHeader(props: Props) {
  return <div className="row">
    <div className="col-sm-12">
      <div className="page-header"><h1>{props.title}</h1></div>
    </div>
  </div>;
}

RoomOccupancyHeader.propTypes = {
  title: PropTypes.string.isRequired
};
