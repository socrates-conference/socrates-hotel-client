//@flow

export default class RoomOccupancyCommand {
  static get GET_PARTICIPANTS_BY_ROOM_TYPE(): string {
    return 'RoomOccupancyCommand/GET_PARTICIPANTS_BY_ROOM_TYPE';
  }
  static get DOWNLOAD_BY_ROOM_TYPE(): string {
    return 'RoomOccupancyCommand/DOWNLOAD_BY_ROOM_TYPE';
  }
  static get GET_ROOM_TYPES(): string {
    return 'RoomOccupancyCommand/GET_ROOM_TYPES';
  }
  static get CHANGE_ROOM_TYPE(): string {
    return 'RoomOccupancyCommand/CHANGE_ROOM_TYPE';
  }
}

export const getParticipantsByRoomType =
  (roomType: string) => ({type: RoomOccupancyCommand.GET_PARTICIPANTS_BY_ROOM_TYPE, roomType});
export const downloadByRoomType =
  (roomType: string) => ({type: RoomOccupancyCommand.DOWNLOAD_BY_ROOM_TYPE, roomType});
export const getRoomTypes = () => ({type: RoomOccupancyCommand.GET_ROOM_TYPES});
export const changeRoomTypeFilter = (roomType: string) => ({type: RoomOccupancyCommand.CHANGE_ROOM_TYPE, roomType});
