// @flow

import {shallow} from 'enzyme/build';
import RoomTypesFilter from './RoomTypesFilter';
import React from 'react';
import type {RoomType} from './roomOccupancyReducer';

describe('(Component) RoomTypesFilter', () => {

  const roomTypes: Array<RoomType> = [
    {
      id: 'room1',
      conferenceEditionId: 1,
      display: 'room1',
      nightPricePerPerson: 10,
      numberOfRooms: 25,
      peoplePerRoom: 1,
      reservedOrganization: 0,
      reservedSponsors: 0,
      lotterySortOrder: 1
    },
    {
      id: 'room2',
      conferenceEditionId: 1,
      display: 'room2',
      nightPricePerPerson: 30,
      numberOfRooms: 12,
      peoplePerRoom: 1,
      reservedOrganization: 0,
      reservedSponsors: 0,
      lotterySortOrder: 2
    }
  ];
  const wrapper = shallow(<RoomTypesFilter default="" roomTypes={roomTypes} filterChanged={() => {}}/>);

  it('renders without exploding', () => {
    expect(wrapper).toHaveLength(1);
  });

  it('renders correctly', () => {
    expect(wrapper).toMatchSnapshot();
  });
});
