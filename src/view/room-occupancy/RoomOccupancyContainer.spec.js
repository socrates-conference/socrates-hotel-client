// @flow

import {shallow} from 'enzyme/build';
import React from 'react';
import RoomOccupancyContainer from './RoomOccupancyContainer';
import StoreFactory, {HistoryFactory} from '../../store/store';
import reducers from '../../rootReducer';
import rootSaga from '../../rootSaga';
import type {Address, CompanyName, ParticipantName, ParticipantRoomOccupancy, RoomType} from './roomOccupancyReducer';

describe('(Component) RoomOccupancyHeader', () => {

  const history = HistoryFactory.createHistory();
  const store = StoreFactory.createStore(reducers, rootSaga, history);

  const roomTypes: Array<RoomType> = [
    {
      id: 'room1',
      conferenceEditionId: 1,
      display: 'room1',
      nightPricePerPerson: 10,
      numberOfRooms: 25,
      peoplePerRoom: 1,
      reservedOrganization: 0,
      reservedSponsors: 0,
      lotterySortOrder: 1
    },
    {
      id: 'room2',
      conferenceEditionId: 1,
      display: 'room2',
      nightPricePerPerson: 30,
      numberOfRooms: 12,
      peoplePerRoom: 1,
      reservedOrganization: 0,
      reservedSponsors: 0,
      lotterySortOrder: 2
    }
  ];

  const roomOccupancies: ParticipantRoomOccupancy[] = [
    {
      name: {firstName: 'first1', lastName: 'last1'},
      companyName: {main: 'company1', additional: 'additional'},
      address: {
        address1: 'adr1',
        address2: 'adr2',
        province: 'pro',
        postal: 'pos',
        city: 'city',
        country: 'country'
      },
      arrival: 'arr',
      departure: 'dep'
    },
    {
      name: {firstName: 'first2', lastName: 'last2'},
      companyName: {main: 'company2'},
      address: {
        address1: 'adr1',
        postal: 'pos',
        city: 'city',
        country: 'country'
      },
      arrival: 'arr',
      departure: 'dep'
    }
  ];

  const wrapper = shallow(<RoomOccupancyContainer store={store}
    roomTypes={roomTypes}
    changeRoomTypeFilter={() => {}}
    getParticipantsByRoomType={() => {}}
    getRoomTypes={() => {}}
    roomOccupancies={roomOccupancies}
    selectedRoomTypeId="room1"
  />);

  it('renders without exploding', () => {
    expect(wrapper).toHaveLength(1);
  });

  it('renders correctly', () => {
    expect(wrapper).toMatchSnapshot();
  });
});
