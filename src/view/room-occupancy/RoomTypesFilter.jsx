// @flow

import React from 'react';
import * as PropTypes from 'prop-types';
import type {RoomType} from './roomOccupancyReducer';

export type Props = {
  filterChanged: (string) => void,
  roomTypes: Array<RoomType>,
  default: string
}

export default function RoomTypesFilter (props: Props) {

  return (
    <div id="select-room-type-filter" className="row mb-2">
      <div className="col-lg-4 col-sm-12 align-self-center">
        <label htmlFor="select-room-type">Select the room type to show: </label>
      </div>
      <div className="col-lg-8 col-sm-12">
        <select
          id="select-room-type" className="custom-select"
          onChange={(e: SyntheticEvent<HTMLSelectElement>) => props.filterChanged(e.currentTarget.value)}
        >
          {
            props.roomTypes && props.roomTypes.map((item) => {
              return (
                <option key={`room-type-${item.id}`} value={item.id} selected={item.id === props.default}>
                  {item.display}
                </option>
              );
            })
          }
        </select>
      </div>
    </div>
  );
}
RoomTypesFilter.propTypes = {
  filterChanged: PropTypes.func.isRequired,
  roomTypes: PropTypes.array.isRequired
};

