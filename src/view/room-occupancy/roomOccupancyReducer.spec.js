import {
  getParticipantRoomOccupancyFailed,
  getParticipantRoomOccupancySucceeded, getRoomTypesFailed,
  getRoomTypesSucceeded, roomTypeFilterChanged
} from './roomOccupancyEvents';
import {INITIAL_STATE, roomOccupancyReducer} from './roomOccupancyReducer';

describe('Room occupancy reducer', () => {
  it('stores participants when GET_PARTICIPANT_ROOM_OCCUPANCY_SUCCEEDED', () => {
    const event = getParticipantRoomOccupancySucceeded([
      {id: 4711, name: {firstName: 'First', lastName: 'Last'}, address: {address1: 'address1',
        postal: '99999', city: 'city', country: 'country'}}
    ]);
    const result = roomOccupancyReducer(INITIAL_STATE, event);
    expect(result.roomOccupancies).toBeDefined();
    expect(result.roomOccupancies).toHaveLength(1);
  });
  it('stores empty array when GET_PARTICIPANT_ROOM_OCCUPANCY_FAILED', () => {
    const event = getParticipantRoomOccupancyFailed();
    const result = roomOccupancyReducer(INITIAL_STATE, event);
    expect(result.roomOccupancies).toBeDefined();
    expect(result.roomOccupancies).toHaveLength(0);
  });
  it('stores participants when GET_ROOM_TYPES_SUCCEEDED', () => {
    const event = getRoomTypesSucceeded([
      {id: 1, conferenceEditionId: 1, display: 'Type 1'},
      {id: 2, conferenceEditionId: 1, display: 'Type 2'},
      {id: 3, conferenceEditionId: 1, display: 'Type 3'},
      {id: 4, conferenceEditionId: 1, display: 'Type 4'}
    ]);
    const result = roomOccupancyReducer(INITIAL_STATE, event);
    expect(result.roomTypes).toBeDefined();
    expect(result.roomTypes).toHaveLength(4);
  });
  it('stores array with \'all\' item when GET_ROOM_TYPES_FAILED', () => {
    const event = getRoomTypesFailed();
    const result = roomOccupancyReducer(INITIAL_STATE, event);
    expect(result.roomTypes).toBeDefined();
    expect(result.roomTypes).toHaveLength(0);
  });
  it('stores selected room type when ROOM_TYPE_FILTER_CHANGED', () => {
    const event = roomTypeFilterChanged('bedInDouble');
    const result = roomOccupancyReducer(INITIAL_STATE, event);
    expect(result.selectedRoomTypeId).toBeDefined();
    expect(result.selectedRoomTypeId).toEqual('bedInDouble');
  });
});
