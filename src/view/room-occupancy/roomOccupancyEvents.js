// @flow

import type {ParticipantRoomOccupancy, RoomType} from './roomOccupancyReducer';

export default class RoomOccupancyEvents {
  static get GET_PARTICIPANT_ROOM_OCCUPANCY_SUCCEEDED() {
    return 'RoomOccupancyEvents/GET_PARTICIPANT_ROOM_OCCUPANCY_SUCCEEDED';
  }

  static get GET_PARTICIPANT_ROOM_OCCUPANCY_FAILED() {
    return 'RoomOccupancyEvents/GET_PARTICIPANT_ROOM_OCCUPANCY_FAILED';
  }

  static get GET_ROOM_TYPES_SUCCEEDED() {
    return 'RoomOccupancyEvents/GET_ROOM_TYPES_SUCCEEDED';
  }

  static get GET_ROOM_TYPES_FAILED() {
    return 'RoomOccupancyEvents/GET_ROOM_TYPES_FAILED';
  }
  static get ROOM_TYPE_FILTER_CHANGED() {
    return 'RoomOccupancyEvents/ROOM_TYPE_FILTER_CHANGED';
  }
}

export const getParticipantRoomOccupancySucceeded =
  (participantRoomOccupancies: ParticipantRoomOccupancy[]) =>
    ({type: RoomOccupancyEvents.GET_PARTICIPANT_ROOM_OCCUPANCY_SUCCEEDED, participantRoomOccupancies});
export const getParticipantRoomOccupancyFailed = () =>
  ({type: RoomOccupancyEvents.GET_PARTICIPANT_ROOM_OCCUPANCY_FAILED});
export const getRoomTypesSucceeded =
  (roomTypes: RoomType[]) => ({type: RoomOccupancyEvents.GET_ROOM_TYPES_SUCCEEDED, roomTypes});
export const getRoomTypesFailed =
  () => ({type: RoomOccupancyEvents.GET_ROOM_TYPES_FAILED});
export const roomTypeFilterChanged =
  (roomType: string) => ({type: RoomOccupancyEvents.ROOM_TYPE_FILTER_CHANGED, roomType});
