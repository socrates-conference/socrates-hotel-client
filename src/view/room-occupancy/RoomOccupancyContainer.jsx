// @flow

import React, {Component} from 'react';
import * as PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {Action, ColumnDefinition, Table} from 'socrates-ui-components';
import RoomOccupancyHeader from './RoomOccupancyHeader';
import type {Address, CompanyName, ParticipantName, ParticipantRoomOccupancy, RoomType} from './roomOccupancyReducer';
import RoomTypesFilter from './RoomTypesFilter';
import {
  changeRoomTypeFilter,
  downloadByRoomType,
  getParticipantsByRoomType,
  getRoomTypes
} from './roomOccupancyCommand';

export type Props = {
  selectedRoomTypeId: string,
  roomOccupancies: ParticipantRoomOccupancy[],
  getParticipantsByRoomType: (string) => void,
  downloadByRoomType: (string) => void,
  getRoomTypes: () => void,
  changeRoomTypeFilter: (string) => void,
  roomTypes: RoomType[]
}

class RoomOccupancyContainer extends Component<Props> {

  static propTypes = {
    changeRoomTypeFilter: PropTypes.func.isRequired,
    downloadByRoomType: PropTypes.func.isRequired,
    getParticipantsByRoomType: PropTypes.func.isRequired,
    getRoomTypes: PropTypes.func.isRequired,
    roomOccupancies: PropTypes.array.isRequired,
    selectedRoomTypeId: PropTypes.string.isRequired
  };

  _nameCellTemplate = (name: ParticipantName) =>
    <div>
      {name.firstName} {name.lastName}
    </div>;

  _companyCellTemplate = (name: CompanyName) =>
    <div>
      <div>
        {name.main}
      </div>
      {name.additional && <div>
        {name.additional}
      </div>}
    </div>;

  _addressCellTemplate = (address: Address) =>
    <div>
      <div>
        {address.address1}
      </div>
      {address.address2 && <div>
        {address.address2}
      </div>}
      {address.province && <div>
        {address.province}
      </div>}
      <div>
        {address.postal}&nbsp;{address.city}
      </div>
      <div>
        {address.country}
      </div>
    </div>;


  _tableActions: Array<Action> = [
    {
      execute: () => this.props.getParticipantsByRoomType(this.props.selectedRoomTypeId),
      canExecute: () => true, buttonClass: 'btn-info',
      icon: 'sync', text: 'Refresh', visible: true
    },
    {
      execute: () => this.props.downloadByRoomType(this.props.selectedRoomTypeId),
      canExecute: () => true, buttonClass: 'btn-primary',
      icon: 'download', text: 'Download CSV File', visible: true
    }
  ];

  _columns: Array<ColumnDefinition> = [
    {header: 'Name', field: 'name', visible: true, cellTemplate: (name) => this._nameCellTemplate(name)},
    {header: 'Company', field: 'companyName', visible: true,
      cellTemplate: (companyName) => this._companyCellTemplate(companyName)},
    {header: 'Address', field: 'address', visible: true,
      cellTemplate: (address) => this._addressCellTemplate(address)},
    {header: 'Arrival', field: 'arrival', visible: true},
    {header: 'Departure', field: 'departure', visible: true}

  ];

  _defaultRoomType = 'single';

  componentDidMount = () => {
    this.props.getRoomTypes();
    this.props.getParticipantsByRoomType(this._defaultRoomType);
  };

  _roomTypeChanged = (roomType: string) => {
    this.props.changeRoomTypeFilter(roomType);
    this.props.getParticipantsByRoomType(roomType);
  };

  render = () => {
    const {roomOccupancies, roomTypes} = this.props;
    return (
      <div className="container">
        <RoomOccupancyHeader title="Room occupancy list"/>
        <RoomTypesFilter filterChanged={this._roomTypeChanged} roomTypes={roomTypes} default={this._defaultRoomType}/>
        <Table
          columns={this._columns} data={roomOccupancies}
          tableActions={this._tableActions}
        />
      </div>
    );
  };
}

const mapStateToProps = (state) => {
  return {...state.roomOccupancy};
};

const mapDispatchToProps = {
  getRoomTypes, getParticipantsByRoomType, changeRoomTypeFilter, downloadByRoomType
};

export default connect(mapStateToProps, mapDispatchToProps)(RoomOccupancyContainer);
