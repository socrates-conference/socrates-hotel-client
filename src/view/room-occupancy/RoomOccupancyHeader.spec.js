import {shallow} from 'enzyme/build';
import React from 'react';
import RoomOccupancyHeader from './RoomOccupancyHeader';

describe('(Component) RoomOccupancyHeader', () => {

  const wrapper = shallow(<RoomOccupancyHeader title="TITLE"/>);

  it('renders without exploding', () => {
    expect(wrapper).toHaveLength(1);
  });

  it('renders correctly', () => {
    expect(wrapper).toMatchSnapshot();
  });
});
