// @flow

import RoomOccupancyEvents from './roomOccupancyEvents';

export type Participant = {
  participantId: ?number,
  personId: number,
  nickname: string,
  email: string,
  firstname: string,
  lastname: string,
  company: string,
  confirmed: boolean,
  address1: string,
  address2: string,
  province: string,
  postal: string,
  city: string,
  country: string,
  arrival: string,
  departure: string,
  dietary: boolean,
  dietaryInfo: string,
  family: boolean,
  familyInfo: string,
  gender: string,
  tshirt: string,
  labelname: string,
  social: string,
  pronoun: string,
  sharedRoom: boolean,
};

export type Address = {
  address1: string,
  address2?: string,
  province?: string,
  postal: string,
  city: string,
  country: string
};

export type ParticipantName = { firstName: string, lastName: string };

export type CompanyName = { main: ?string, additional?: string };

export type ParticipantRoomOccupancy = {
  name: ParticipantName,
  companyName: CompanyName,
  address: Address,
  arrival: string,
  departure: string
}

export type RoomType = {
  id: string,
  conferenceEditionId: number,
  display: string,
  nightPricePerPerson: number,
  numberOfRooms: number,
  peoplePerRoom: number,
  reservedOrganization: number,
  reservedSponsors: number,
  lotterySortOrder: number
}

export type RoomOccupancyState = {
  roomOccupancies: ParticipantRoomOccupancy[],
  roomTypes: RoomType[],
  selectedRoomTypeId: string
}

export const INITIAL_STATE: RoomOccupancyState = {
  roomOccupancies: [],
  roomTypes: [],
  selectedRoomTypeId: 'single'

};

export const roomOccupancyReducer = (state: RoomOccupancyState = INITIAL_STATE, action: any) => {
  switch (action.type) {
    case RoomOccupancyEvents.GET_PARTICIPANT_ROOM_OCCUPANCY_SUCCEEDED:
      return {...state, roomOccupancies: action.participantRoomOccupancies};
    case RoomOccupancyEvents.GET_PARTICIPANT_ROOM_OCCUPANCY_FAILED:
      return {...state, roomOccupancies: []};
    case RoomOccupancyEvents.GET_ROOM_TYPES_SUCCEEDED:
      return {...state, roomTypes: action.roomTypes};
    case RoomOccupancyEvents.GET_ROOM_TYPES_FAILED:
      return {...state, roomTypes: []};
    case RoomOccupancyEvents.ROOM_TYPE_FILTER_CHANGED:
      return {...state, selectedRoomTypeId: action.roomType};
    default:
      return state;
  }
};

export default {roomOccupancy: roomOccupancyReducer};
