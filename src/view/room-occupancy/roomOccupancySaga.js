// @flow
import * as api from '../../requests/api';
import {call, put} from 'redux-saga/effects';
import {
  getParticipantRoomOccupancySucceeded,
  getRoomTypesFailed,
  getRoomTypesSucceeded,
  roomTypeFilterChanged
} from './roomOccupancyEvents';
import type {
  Participant,
  ParticipantRoomOccupancy,
  RoomType
} from './roomOccupancyReducer';

export function* getRoomTypesSaga(): Iterable<any> {
  const roomTypes: ?RoomType[] = yield call(api.readRoomTypes);
  if (roomTypes && roomTypes.length > 0) {
    yield put(getRoomTypesSucceeded(roomTypes));
  } else {
    yield put(getRoomTypesFailed());
  }
}

export function* getParticipantsByRoomTypeSaga(action: Object): Iterable<any> {
  const participants: ?Participant[] = yield call(api.readParticipantsByRoomType, action.roomType);
  if (participants) {
    const participantRoomOccupancy: ParticipantRoomOccupancy[] = participants.map(item => ({
      name: {firstName: item.firstname, lastName: item.lastname},
      companyName: {main: item.company, additional: ''},
      address: {address1: item.address1, address2: item.address2, province: item.province,
        postal: item.postal, city: item.city, country: item.country},
      arrival: item.arrival,
      departure: item.departure
    }));
    yield put(getParticipantRoomOccupancySucceeded(participantRoomOccupancy));
  }
}
export function* downloadParticipantsByRoomTypeFileSaga(action: Object): Iterable<any> {
  yield call(api.downloadFile, action.roomType);
}

export function* changeRoomTypeSaga(action: Object): Iterable<any> {
  yield put(roomTypeFilterChanged(action.roomType));
}
