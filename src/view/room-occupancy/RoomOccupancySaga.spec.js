// @flow
import axios from 'axios';
import SagaTester from 'redux-saga-tester';
import rootSaga from '../../rootSaga';
import type {ParticipantRoomOccupancy, RoomType} from './roomOccupancyReducer';
import {INITIAL_STATE, roomOccupancyReducer} from './roomOccupancyReducer';
import RoomOccupancyEvents from './roomOccupancyEvents';
import {
  changeRoomTypeFilter,
  downloadByRoomType,
  getParticipantsByRoomType,
  getRoomTypes
} from './roomOccupancyCommand';

jest.mock('axios');

const roomTypes: Array<RoomType> = [
  {
    id: 'room1',
    conferenceEditionId: 1,
    display: 'room1',
    nightPricePerPerson: 10,
    numberOfRooms: 25,
    peoplePerRoom: 1,
    reservedOrganization: 0,
    reservedSponsors: 0,
    lotterySortOrder: 1
  },
  {
    id: 'room2',
    conferenceEditionId: 1,
    display: 'room2',
    nightPricePerPerson: 30,
    numberOfRooms: 12,
    peoplePerRoom: 1,
    reservedOrganization: 0,
    reservedSponsors: 0,
    lotterySortOrder: 2
  }
];

const roomOccupancies: ParticipantRoomOccupancy[] = [
  {
    name: {firstName: 'first1', lastName: 'last1'},
    companyName: {main: 'company1', additional: 'additional'},
    address: {
      address1: 'adr1',
      address2: 'adr2',
      province: 'pro',
      postal: 'pos',
      city: 'city',
      country: 'country'
    },
    arrival: 'arr',
    departure: 'dep'
  },
  {
    name: {firstName: 'first2', lastName: 'last2'},
    companyName: {main: 'company2'},
    address: {
      address1: 'adr1',
      postal: 'pos',
      city: 'city',
      country: 'country'
    },
    arrival: 'arr',
    departure: 'dep'
  }
];


describe('om Occupancy saga', () => {
  let sagaTester: SagaTester;
  let promise;
  beforeEach(() => {
    sagaTester = new SagaTester({INITIAL_STATE, reducers: roomOccupancyReducer});
    sagaTester.start(rootSaga);
  });

  afterEach(() => {
    sagaTester.reset();
  });

  describe('getting room types', () => {
    beforeEach(() => {
      axios.get.mockImplementation(() => promise);
    });

    afterEach(() => {
      axios.get.mockReset();
    });

    it('dispatches GET_ROOM_TYPES_SUCCEEDED', async () => {
      promise = Promise.resolve({data: roomTypes});
      sagaTester.dispatch(getRoomTypes());
      await sagaTester.waitFor(RoomOccupancyEvents.GET_ROOM_TYPES_SUCCEEDED);
    });
    it('dispatches GET_ROOM_TYPES_FAILED, when error', async () => {
      promise = Promise.reject({message: 'error'});
      sagaTester.dispatch(getRoomTypes());
      await sagaTester.waitFor(RoomOccupancyEvents.GET_ROOM_TYPES_FAILED);
    });
    it('dispatches GET_ROOM_TYPES_FAILED, when null', async () => {
      promise = Promise.resolve({data: null});
      sagaTester.dispatch(getRoomTypes());
      await sagaTester.waitFor(RoomOccupancyEvents.GET_ROOM_TYPES_FAILED);
    });
  });
  describe('getting participants per room type', () => {
    beforeEach(() => {
      axios.get.mockImplementation(() => promise);
    });

    afterEach(() => {
      axios.get.mockReset();
    });

    it('dispatches GET_PARTICIPANT_ROOM_OCCUPANCY_SUCCEEDED', async () => {
      promise = Promise.resolve({data: roomOccupancies});
      sagaTester.dispatch(getParticipantsByRoomType('single'));
      await sagaTester.waitFor(RoomOccupancyEvents.GET_PARTICIPANT_ROOM_OCCUPANCY_SUCCEEDED);
    });
    it('dispatches GET_PARTICIPANT_ROOM_OCCUPANCY_SUCCEEDED, when error', async () => {
      promise = Promise.reject({message: 'error'});
      sagaTester.dispatch(getParticipantsByRoomType('single'));
      await sagaTester.waitFor(RoomOccupancyEvents.GET_PARTICIPANT_ROOM_OCCUPANCY_SUCCEEDED);
    });
    it('dispatches GET_PARTICIPANT_ROOM_OCCUPANCY_SUCCEEDED, when null', async () => {
      promise = Promise.resolve({data: null});
      sagaTester.dispatch(getParticipantsByRoomType('single'));
      await sagaTester.waitFor(RoomOccupancyEvents.GET_PARTICIPANT_ROOM_OCCUPANCY_SUCCEEDED);
    });
  });
  describe('updating selected room type', () => {
    it('dispatches ROOM_TYPE_FILTER_CHANGED', async () => {
      sagaTester.dispatch(changeRoomTypeFilter('single'));
      await sagaTester.waitFor(RoomOccupancyEvents.ROOM_TYPE_FILTER_CHANGED);
    });
  });
  describe('download participant file', () => {
    beforeEach(() => {
      axios.get.mockImplementationOnce(() => promise);
    });

    afterEach(() => {
      axios.get.mockReset();
    });

    it('dispatches DOWNLOAD_FILE_SUCCEEDED', async () => {
      const result = 'FirstName\tLastName\tCompany\tAddress1\tAddress2\tProvince\tZIP\tCity\tCountry' +
        '\tArrivalTime\tDepartureTime\tHasFamily\tFamilyInfo\tDietaryNeeds\tDietaryInfo\nPeter\tMuster\t' +
        'My Company GmbH\tThomasstraße 666\t\t\t99999\tcity\tcountry\tThursday afternoon\tMonday\ttrue\t' +
        'family info\ttrue\tDietaryNeeds. With two lines.\n';
      promise = Promise.resolve({data: result});
      sagaTester.dispatch(downloadByRoomType('single'));
    });
  });
});
