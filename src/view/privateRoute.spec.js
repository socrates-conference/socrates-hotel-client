import React from 'react';
import {mount} from 'enzyme';
import {MemoryRouter, Route, Switch} from 'react-router';
import PrivateRoute from './PrivateRoute';
import {createStore} from 'redux';

const INITIAL = {
  authentication: {
    token: '',
    userName: 'Guest',
    isAdministrator: false,
    hasFinished: false
  }
};
const authenticatedNoAdmin = {
  authentication: {
    token: 'the token',
    userName: 'Guest',
    isAdministrator: false,
    hasFinished: false
  }
};
const authenticatedAdmin = {
  authentication: {
    token: 'theToken',
    userName: 'Guest',
    isAdministrator: true,
    hasFinished: false
  }
};

const mountRouterTest = (store, needsAdmin) => {
  return mount(
    <MemoryRouter initialEntries={['/management']}>
      <Switch>
        <Route path="/login">
          <div id="login"><h3>login</h3></div>
        </Route>
        <PrivateRoute store={store} path="/management" needsAdmin={needsAdmin}>
          <div id="management"><p>management</p></div>
        </PrivateRoute>
      </Switch>
    </MemoryRouter>
  );
};

describe('Private Component', () => {
  it('renders without exploding', () => {
    const store = createStore((state) => state, INITIAL);
    const wrapper = mountRouterTest(store, false);
    expect(wrapper.length).toBe(1);
  });

  it('redirects to login on unauthenticated access', () => {
    const store = createStore((state) => state, INITIAL);
    const wrapper = mountRouterTest(store, false);
    expect(wrapper.find('#login').length).toBe(1);
    expect(wrapper.find('#management').length).toBe(0);
  });
  it('redirects to login if admin is needed and authenticated user is no admin', () => {
    const store = createStore((state) => state, authenticatedNoAdmin);
    const wrapper = mountRouterTest(store, true);
    expect(wrapper.find('#login').length).toBe(1);
    expect(wrapper.find('#management').length).toBe(0);
  });
  it('renders private page if authenticated and is admin', () => {
    const store = createStore((state) => state, authenticatedAdmin);
    const wrapper = mountRouterTest(store, true);
    expect(wrapper.find('#login').length).toBe(0);
    expect(wrapper.find('#management').length).toBe(1);
  });
});
