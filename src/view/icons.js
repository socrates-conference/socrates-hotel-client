import fontawesome from '@fortawesome/fontawesome/index';
import brands from '@fortawesome/fontawesome-free-brands/index';
import solid from '@fortawesome/fontawesome-free-solid/index';

fontawesome.library.add(brands, solid);
