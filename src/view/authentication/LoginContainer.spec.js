import {shallow} from 'enzyme';
import React from 'react';
import {Login} from './Login';
import {spy} from 'sinon';

const state = {
  token: '',
  result: {isSuccess: false, isAdministrator: false},
  hasFinished: false
};

describe('Login', () => {
  let login;
  const loginSpy = spy();
  beforeEach(() => {
    login = shallow(<Login login={loginSpy} state={state} location={{state: {from: '/'}}}/>);
  });

  afterEach(()=>{
    loginSpy.resetHistory();
  });

  it('renders without crashing', () => {
    expect(login).toHaveLength(1);
  });

  it('renders correctly', () => {
    expect(login).toMatchSnapshot();
  });
});
