// @flow

import React, {Component} from 'react';
import * as PropTypes from 'prop-types';
import {connect} from 'react-redux';
import type {AuthenticationState} from './authenticationReducer';
import {LoginContainer} from 'socrates-ui-components';
import './authentication.css';
import {login} from './authenticationCommand';
import {withRouter} from 'react-router';


export type Props = {
  from: string,
  location: Object,
  login: (string, string, string) => void,
  state: AuthenticationState;
}

export class Login extends Component<Props> {

  static propTypes = {
    from: PropTypes.string,
    location: PropTypes.object.isRequired,
    login: PropTypes.func.isRequired,
    state: PropTypes.any
  };

  login = (email: string, password: string) => {
    const redirectGoal =
      this.props.location.state && this.props.location.state.from ? this.props.location.state.from : '/home';
    this.props.login(email, password, redirectGoal);
  };

  render = () => {
    const showErrorMessage = this.props.state.hasFinished && this.props.state.token.trim().length === 0;
    return (
      <div id="login">
        <LoginContainer
          login={this.login}
          showErrorMessage={showErrorMessage}
        />
      </div>
    );
  };
}

const mapStateToProps = (state) => {
  return {state: {...state.authentication}};
};

const mapDispatchToProps = {
  login
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Login));
