// @flow
import {put, call} from 'redux-saga/effects';
import {loginError, loginStarted, loginSuccess, logoutSuccess} from './authenticationEvents';
import {routeTo} from '../routingCommand';
import config from '../../config/config';
import * as api from '../../requests/api';
import * as jwt from 'jsonwebtoken';
import * as axios from 'axios/index';
import * as sjcl from 'sjcl';

export type LoginCommandData = {
  email: string,
  password: string,
  comesFrom: string
}

export function* loginSaga(action: LoginCommandData): Iterable<any> {
  yield put(loginStarted());
  try {

    const sha256 = sjcl.hash.sha256.hash(action.password);
    const hash = sjcl.codec.hex.fromBits(sha256);
    const payload = {
      email: action.email,
      hash: hash
    };
    const requestToken = yield call(jwt.sign, payload, config.jwtSecret, {expiresIn: 15});



    const token: ?string = yield call(api.login, requestToken);
    if (token && token.trim().length > 0) {
      const userData = yield call(jwt.verify, token, config.jwtSecret);
      if (userData) {
        axios.defaults.headers.common['Authorization'] = `Bearer ${token}`;
        yield put(loginSuccess(token, userData));
        yield put(routeTo(action.comesFrom));
      } else {
        yield put(loginError());
      }
    } else {
      yield put(loginError());
    }
  } catch (error) {
    yield put(loginError());
  }
}

export function* logoutSaga(): Iterable<any> {
  delete axios.defaults.headers.common['Authorization'];
  yield put(logoutSuccess());
}

