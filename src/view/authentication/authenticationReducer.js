// @flow

import AuthenticationEvent from './authenticationEvents';

export type AuthenticationState = {
  token: string,
  isAdministrator: boolean,
  isHotel: boolean,
  userName: string,
  hasFinished: boolean,
}

const INITIAL: AuthenticationState = {
  token: '',
  userName: 'Guest',
  isAdministrator: false,
  isHotel: false,
  hasFinished: false
};

const authenticationReducer = (state: AuthenticationState = INITIAL, action: any) => {
  switch (action.type) {
    case AuthenticationEvent.LOGIN_SUCCESS:
      return {
        ...state,
        hasFinished: true,
        token: action.token,
        isAdministrator: action.data.isAdministrator,
        isHotel: action.data.isHotel,
        userName: action.data.name
      };
    case AuthenticationEvent.LOGIN_STARTED:
    case AuthenticationEvent.LOGOUT_SUCCESS:
      return {...state, isAdministrator: false, hasFinished: false, token: '', userName: 'Guest'};
    case AuthenticationEvent.LOGIN_ERROR:
      return {...state, isAdministrator: false, hasFinished: true, token: '', userName: 'Guest'};
    default:
      return state;
  }
};

export default {authentication: authenticationReducer};
