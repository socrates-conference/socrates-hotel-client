//@flow

export default class AuthenticationCommand {
  static get LOGIN(): string {
    return 'Command/AUTHENTICATION_LOGIN';
  }
  static get LOGOUT(): string {
    return 'Command/AUTHENTICATION_LOGOUT';
  }
}

export const login = (email: string, password: string, comesFrom: string) => ({
  type: AuthenticationCommand.LOGIN,
  email,
  password,
  comesFrom
});

export const logout = () => ({type: AuthenticationCommand.LOGOUT});
