// @flow

import React from 'react';
import {shallow} from 'enzyme/build';
import {Home} from './Home';
import type {
  DailyFee,
  ParticipantArrival,
  ParticipantArrivalsAndDepartures,
  ParticipantDeparture
} from './dashboardReducer';

describe('(Component) Home', () => {

  const hints = {
    arrivals: [{id: 'one', name: 'Arrival one', hint: 'Hint arrival one'}],
    departures: [{id: 'one', name: 'Departure one', hint: 'Hint departure one'}]
  };

  const participantArrivals: ParticipantArrival[] = [
    {arrival: '0', arrivalText: 'arrival One', count: 20},
    {arrival: '1', arrivalText: 'arrival Two', count: 10}
  ];
  const participantDepartures: ParticipantDeparture[] = [
    {departure: '0', departureText: 'arrival One', count: 13},
    {departure: '1', departureText: 'arrival Two', count: 17},
    {departure: '1', departureText: 'arrival Two', count: 55}
  ];

  const arrivalsAndDepartures: ParticipantArrivalsAndDepartures = {
    arrivals: participantArrivals,
    departures: participantDepartures
  };

  const dailyFees: DailyFee[] = [
    {conferenceDayId: 1, conferenceDayNumber: 1, conferenceDayName: 'Day 1', amount: 10},
    {conferenceDayId: 2, conferenceDayNumber: 2, conferenceDayName: 'Day 2', amount: 15},
    {conferenceDayId: 3, conferenceDayNumber: 3, conferenceDayName: 'Day 3', amount: 20}
  ];

  const wrapper = shallow(
    <Home
      getArrivalsAndDeparturesHints={() => {}} hints={hints}
      getArrivalsAndDepartures={() => {}} arrivalsAndDepartures={arrivalsAndDepartures}
      getTotalAmountOfParticipants={() => {}} totalParticipants={200}
      getDailyFeesPerDay={() => {}} dailyFees={dailyFees}
    />);

  it('renders without exploding', () => {
    expect(wrapper).toHaveLength(1);
  });

  it('renders correctly', () => {
    expect(wrapper).toMatchSnapshot();
  });
});
