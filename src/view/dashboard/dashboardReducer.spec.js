// @flow

import {dashboardReducer, INITIAL_STATE} from './dashboardReducer';
import type {
  Hints,
  ParticipantArrival,
  ParticipantArrivalsAndDepartures,
  ParticipantDeparture
} from './dashboardReducer';
import {
  getArrivalsAndDeparturesFailed,
  getArrivalsAndDeparturesSucceeded,
  getTotalAmountOfParticipantsSucceeded,
  getHintsFailed,
  getHintsSucceeded, getDailyFeesPerDaySucceeded
} from './dashboardEvents';

const hints: Hints = {
  arrivals: [{id: 'one', name: 'Arrival one', hint: 'Hint arrival one'}],
  departures: [{id: 'one', name: 'Departure one', hint: 'Hint departure one'}]
};

const participantArrivals: ParticipantArrival[] = [
  {arrival: '0', arrivalText: 'arrival One', count: 20},
  {arrival: '1', arrivalText: 'arrival Two', count: 10}
];
const participantDepartures: ParticipantDeparture[] = [
  {departure: '0', departureText: 'arrival One', count: 13},
  {departure: '1', departureText: 'arrival Two', count: 17},
  {departure: '1', departureText: 'arrival Two', count: 55}
];

const arrivalsAndDepartures: ParticipantArrivalsAndDepartures = {
  arrivals: participantArrivals,
  departures: participantDepartures
};

const dailyFees = [
  {conferenceDayId: 1, conferenceDayNumber: 1, conferenceDayName: 'Day 1', amount: 10},
  {conferenceDayId: 2, conferenceDayNumber: 2, conferenceDayName: 'Day 2', amount: 15},
  {conferenceDayId: 3, conferenceDayNumber: 3, conferenceDayName: 'Day 3', amount: 20}
];

describe('Dashboard reducer', () => {
  it('stores hints when GET_HINTS_SUCCEEDED', () => {
    const event = getHintsSucceeded(hints);
    const result = dashboardReducer(INITIAL_STATE, event);
    expect(result.hints).toBeDefined();
    expect(result.hints).toEqual(hints);
    expect(result.hints.arrivals).toHaveLength(1);
    expect(result.hints.departures).toHaveLength(1);
  });
  it('stores empty arrivals and departures hints when GET_HINTS_FAILED', () => {
    const event = getHintsFailed(new Error('Error'));
    const result = dashboardReducer(INITIAL_STATE, event);
    expect(result.hints).toBeDefined();
    expect(result.hints.arrivals).toHaveLength(0);
    expect(result.hints.departures).toHaveLength(0);

  });
  it('stores amount of participants when GET_TOTAL_AMOUNT_OF_PARTICIPANTS_SUCCEEDED', () => {
    const event = getTotalAmountOfParticipantsSucceeded(200);
    const result = dashboardReducer(INITIAL_STATE, event);
    expect(result.totalParticipants).toBe(200);
  });
  it('stores arrivals and departures when GET_ARRIVALS_ANS_DEPARTURES_SUCCEEDED', () => {
    const event = getArrivalsAndDeparturesSucceeded(arrivalsAndDepartures);
    const result = dashboardReducer(INITIAL_STATE, event);
    expect(result.arrivalsAndDepartures).toEqual(arrivalsAndDepartures);
  });
  it('stores empty arrivals and departures when GET_ARRIVALS_ANS_DEPARTURES_FAILED', () => {
    const event = getArrivalsAndDeparturesFailed(new Error('Error'));
    const result = dashboardReducer(INITIAL_STATE, event);
    expect(result.arrivalsAndDepartures).toBeDefined();
    expect(result.arrivalsAndDepartures.arrivals).toHaveLength(0);
    expect(result.arrivalsAndDepartures.departures).toHaveLength(0);
  });
  it('stores arrivals and departures when GET_ARRIVALS_ANS_DEPARTURES_SUCCEEDED', () => {
    const event = getDailyFeesPerDaySucceeded(dailyFees);
    const result = dashboardReducer(INITIAL_STATE, event);
    expect(result.dailyFees).toEqual(dailyFees);
  });
});
