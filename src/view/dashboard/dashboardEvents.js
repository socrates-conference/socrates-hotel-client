// @flow

import type {DailyFee, Hints, ParticipantArrivalsAndDepartures} from './dashboardReducer';

export default class DashboardEvents {
  static get GET_ARRIVALS_ANS_DEPARTURES_SUCCEEDED() {
    return 'DashboardEvents/GET_ARRIVALS_ANS_DEPARTURES_SUCCEEDED';
  }
  static get GET_ARRIVALS_ANS_DEPARTURES_FAILED() {
    return 'DashboardEvents/GET_ARRIVALS_ANS_DEPARTURES_FAILED';
  }
  static get GET_HINTS_SUCCEEDED() {
    return 'DashboardEvents/GET_CONFERENCE_SUCCEEDED';
  }

  static get GET_HINTS_FAILED() {
    return 'DashboardEvents/GET_CONFERENCE_FAILED';
  }

  static get GET_TOTAL_AMOUNT_OF_PARTICIPANTS_SUCCEEDED() {
    return 'DashboardEvents/GET_TOTAL_AMOUNT_OF_PARTICIPANTS_SUCCEEDED';
  }

  static get GET_DAILY_FEES_PER_DAY_SUCCEEDED() {
    return 'DashboardEvents/GET_DAILY_FEES_PER_DAY_SUCCEEDED';
  }
}

export const getHintsSucceeded = (hints: Hints) => ({type: DashboardEvents.GET_HINTS_SUCCEEDED, hints});
export const getHintsFailed = (error: any) => ({type: DashboardEvents.GET_HINTS_FAILED, error});
export const getArrivalsAndDeparturesSucceeded =
  (arrivalsAndDepartures: ParticipantArrivalsAndDepartures) =>
    ({type: DashboardEvents.GET_ARRIVALS_ANS_DEPARTURES_SUCCEEDED, arrivalsAndDepartures});
export const getArrivalsAndDeparturesFailed =
  (error: any) => ({type: DashboardEvents.GET_ARRIVALS_ANS_DEPARTURES_FAILED, error});
export const getTotalAmountOfParticipantsSucceeded =
  (amountOfParticipants: number) =>
    ({type: DashboardEvents.GET_TOTAL_AMOUNT_OF_PARTICIPANTS_SUCCEEDED, amountOfParticipants});
export const getDailyFeesPerDaySucceeded =
  (dailyFees: DailyFee[]) =>
    ({type: DashboardEvents.GET_DAILY_FEES_PER_DAY_SUCCEEDED, dailyFees});
