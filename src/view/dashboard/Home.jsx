// @flow

import React from 'react';
import {connect} from 'react-redux';
import {getArrivalsAndDeparturesHints, getArrivalsAndDepartures,
  getTotalAmountOfParticipants, getDailyFeesPerDay} from './dashboardCommand';
import type {DailyFee, Hints, ParticipantArrivalsAndDepartures} from './dashboardReducer';
import * as PropTypes from 'prop-types';
// noinspection ES6CheckImport
import {ColumnDefinition, Table} from 'socrates-ui-components';

type Props = {
  getArrivalsAndDeparturesHints: () => void,
  getTotalAmountOfParticipants: () => void,
  getArrivalsAndDepartures: () => void,
  getDailyFeesPerDay: () => void,
  dailyFees: DailyFee[],
  hints: Hints,
  totalParticipants: number,
  arrivalsAndDepartures: ParticipantArrivalsAndDepartures
}

const arrivalHintColumns: ColumnDefinition[] = [
  {header: 'Arrival', field: 'name', visible: true},
  {header: 'Info', field: 'hint', visible: true}
];
const departureHintColumns: ColumnDefinition[] = [
  {header: 'Departure', field: 'name', visible: true},
  {header: 'Info', field: 'hint', visible: true}
];

const arrivalColumns: ColumnDefinition[] = [
  {header: 'Arrival', field: 'arrivalText', visible: true},
  {header: 'Amount of People', field: 'count', visible: true}
];
const departureColumns: ColumnDefinition[] = [
  {header: 'Departure', field: 'departureText', visible: true},
  {header: 'Amount of People', field: 'count', visible: true}
];
const dailyFeesColumns: ColumnDefinition[] = [
  {header: 'Day', field: 'conferenceDayName', visible: true},
  {header: 'Amount', field: 'amount', visible: true}
];

export class Home extends React.Component<Props> {

  static propTypes = {
    arrivalsAndDepartures: PropTypes.object.isRequired,
    dailyFees: PropTypes.array.isRequired,
    getArrivalsAndDepartures: PropTypes.func.isRequired,
    getArrivalsAndDeparturesHints: PropTypes.func.isRequired,
    getDailyFeesPerDay: PropTypes.func.isRequired,
    getTotalAmountOfParticipants: PropTypes.func.isRequired,
    hints: PropTypes.object.isRequired,
    totalParticipants: PropTypes.number.isRequired
  };

  componentDidMount(): void {
    this.props.getArrivalsAndDeparturesHints();
    this.props.getArrivalsAndDepartures();
    this.props.getTotalAmountOfParticipants();
    this.props.getDailyFeesPerDay();
  }

  render = () => {
    const {hints, arrivalsAndDepartures} = this.props;
    return (
      <div className="container">
        <div className="row">
          <div className="col-sm-12">
            <div className="page-header">
              <h1>Overview</h1>
            </div>
            <p>Welcome to the hotel management pages for the SoCraTes conference.</p>
          </div>
        </div>
        <div className="row">
          <div className="col-sm-12">
            <div>
              <h5><b>Meaning of arrivals and departures for the amount of dinners a participant will enjoy.</b></h5>
            </div>
            <div>
              <Table
                columns={arrivalHintColumns} pagination={{isActive: false}}
                data={hints.arrivals ? hints.arrivals : []}/>
            </div>
            <div>
              <Table
                columns={departureHintColumns} pagination={{isActive: false}}
                data={hints.departures ? hints.departures : []}/>
            </div>
          </div>
        </div>
        <div>
          <h5><b>Total amount of participants: {this.props.totalParticipants}</b></h5>
        </div>
        <div className="row">
          <div className="col-sm-12">
            <h5><b>Arrivals and departures</b></h5>
          </div>
          <div className="col-sm-12 col-md-6">
            <div className="col-sm-12">
              <h6>Number of participants by arrival time</h6>
            </div>
            <Table
              columns={arrivalColumns} pagination={{isActive: false}}
              data={arrivalsAndDepartures.arrivals}/>
          </div>
          <div className="col-sm-12 col-md-6">
            <div className="col-sm-12">
              <h6>Number of participants by departure time</h6>
            </div>
            <Table
              columns={departureColumns} pagination={{isActive: false}}
              data={arrivalsAndDepartures.departures}/>
          </div>
        </div>
        <div className="row">
          <div className="col-sm-12 col-md-6">
            <div>
              <h5><b>Conference daily fees per day</b></h5>
            </div>
            <div>
              <Table
                columns={dailyFeesColumns} pagination={{isActive: false}}
                data={this.props.dailyFees ? this.props.dailyFees.filter(x => x.amount > 0) : []}/>
            </div>
          </div>
        </div>
      </div>
    );
  };
}

const mapStateToProps = (state) => {
  return {...state.hotel};
};
const mapDispatchToProps = {
  getArrivalsAndDeparturesHints,
  getTotalAmountOfParticipants,
  getArrivalsAndDepartures,
  getDailyFeesPerDay
};

export default connect(mapStateToProps, mapDispatchToProps)(Home);


