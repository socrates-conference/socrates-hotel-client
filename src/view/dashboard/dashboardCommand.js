//@flow

export default class DashboardCommand {
  static get GET_ARRIVALS_AND_DEPARTURES_HINTS(): string {
    return 'DashboardCommand/GET_ARRIVALS_AND_DEPARTURES_HINTS';
  }
  static get GET_TOTAL_AMOUNT_OF_PARTICIPANTS(): string {
    return 'DashboardCommand/GET_TOTAL_AMOUNT_OF_PARTICIPANTS';
  }
  static get GET_ARRIVALS_AND_DEPARTURES(): string {
    return 'DashboardCommand/GET_ARRIVALS_AND_DEPARTURES';
  }
  static get GET_DAILY_FEES_PER_DAY(): string {
    return 'DashboardCommand/GET_DAILY_FEES_PER_DAY';
  }
}

export const getArrivalsAndDeparturesHints = () => ({type: DashboardCommand.GET_ARRIVALS_AND_DEPARTURES_HINTS});
export const getTotalAmountOfParticipants = () => ({type: DashboardCommand.GET_TOTAL_AMOUNT_OF_PARTICIPANTS});
export const getArrivalsAndDepartures = () => ({type: DashboardCommand.GET_ARRIVALS_AND_DEPARTURES});
export const getDailyFeesPerDay = () => ({type: DashboardCommand.GET_DAILY_FEES_PER_DAY});
