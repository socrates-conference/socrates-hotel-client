// @flow
import * as api from '../../requests/api';
import type {Hints, ParticipantArrivalsAndDepartures} from './dashboardReducer';
import {
  getArrivalsAndDeparturesFailed,
  getArrivalsAndDeparturesSucceeded, getDailyFeesPerDaySucceeded,
  getHintsFailed,
  getHintsSucceeded,
  getTotalAmountOfParticipantsSucceeded
} from './dashboardEvents';
import {call, put} from 'redux-saga/effects';

export function* getHintsSaga(): Iterable<any> {
  let hints: ?Hints;
  try {
    hints = yield call(api.readHints);
  } catch (e) {
    console.error('cannot read hints: ', e);
  }
  if (hints) {
    yield put(getHintsSucceeded(hints));
  } else {
    yield put(getHintsFailed(new Error('cannot read hints.')));
  }
}

export function* getArrivalsAndDeparturesSaga(): Iterable<any> {
  let arrivalsAndDepartures: ?ParticipantArrivalsAndDepartures;
  try {
    arrivalsAndDepartures = yield call(api.readArrivalsAndDepartures);
  } catch (e) {
    console.error('cannot read arrivalsAndDepartures: ', e);
  }
  if (arrivalsAndDepartures) {
    yield put(getArrivalsAndDeparturesSucceeded(arrivalsAndDepartures));
  } else {
    yield put(getArrivalsAndDeparturesFailed(new Error('cannot read arrivals and departures data.')));
  }
}

export function* getTotalAmountOfParticipantsSaga(): Iterable<any> {
  const amount = yield call(api.readAmountOfParticipants);
  yield put(getTotalAmountOfParticipantsSucceeded(amount ? amount : 0));
}

export function* getDailyFeesSaga(): Iterable<any> {
  const dailyFees = yield call(api.readDailyFeesPerDay);
  yield put(getDailyFeesPerDaySucceeded(dailyFees ? dailyFees : []));
}
