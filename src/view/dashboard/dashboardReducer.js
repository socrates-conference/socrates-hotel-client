// @flow

import DashboardEvents from './dashboardEvents';

export type Hint = {
  id: string,
  name: string,
  hint: string
}
export type Hints = {
  arrivals: Hint[],
  departures: Hint[]
}
export type ParticipantArrival = {
  arrival: string,
  arrivalText: string,
  count: number
}

export type ParticipantDeparture = {
  departure: string,
  departureText: string,
  count: number
}

export type ParticipantArrivalsAndDepartures= {
  arrivals: ParticipantArrival[],
  departures: ParticipantDeparture[]
}

export type DailyFee = {
  conferenceDayId: number,
  conferenceDayNumber: number,
  conferenceDayName: string,
  amount: number
}

export type DashboardState = {
  dailyFees: DailyFee[],
  hints: Hints,
  arrivalsAndDepartures: ParticipantArrivalsAndDepartures,
  totalParticipants: number
}

export const INITIAL_STATE: DashboardState = {
  dailyFees: [],
  hints: {arrivals: [], departures: []},
  totalParticipants: 0,
  arrivalsAndDepartures: {arrivals: [], departures: []}
};

export const dashboardReducer = (state: DashboardState = INITIAL_STATE, action: any) => {
  switch (action.type) {
    case DashboardEvents.GET_HINTS_SUCCEEDED:
      return {...state, hints: action.hints};
    case DashboardEvents.GET_HINTS_FAILED:
      return {...state, hints: {arrivals: [], departures: []}};
    case DashboardEvents.GET_TOTAL_AMOUNT_OF_PARTICIPANTS_SUCCEEDED:
      return {...state, totalParticipants: action.amountOfParticipants};
    case DashboardEvents.GET_DAILY_FEES_PER_DAY_SUCCEEDED:
      return {...state, dailyFees: action.dailyFees};
    case DashboardEvents.GET_ARRIVALS_ANS_DEPARTURES_SUCCEEDED:
      return {...state, arrivalsAndDepartures: action.arrivalsAndDepartures};
    case DashboardEvents.GET_ARRIVALS_ANS_DEPARTURES_FAILED:
      return {...state, arrivalsAndDepartures: {arrivals: [], departures: []}};
    default:
      return state;
  }
};

export default {hotel: dashboardReducer};
