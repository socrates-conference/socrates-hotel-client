// @flow
import axios from 'axios';
import SagaTester from 'redux-saga-tester';
import rootSaga from '../../rootSaga';
import dashboardReducer, {INITIAL_STATE} from './dashboardReducer';
import type {
  Hints,
  ParticipantArrival,
  ParticipantArrivalsAndDepartures,
  ParticipantDeparture
} from './dashboardReducer';
import {
  getArrivalsAndDepartures,
  getArrivalsAndDeparturesHints,
  getDailyFeesPerDay,
  getTotalAmountOfParticipants
} from './dashboardCommand';
import DashboardEvents from './dashboardEvents';

jest.mock('axios');

const hints: Hints = {
  arrivals: [{id: 'one', name: 'Arrival one', hint: 'Hint arrival one'}],
  departures: [{id: 'one', name: 'Departure one', hint: 'Hint departure one'}]
};

const participantArrivals: ParticipantArrival[] = [
  {arrival: '0', arrivalText: 'arrival One', count: 20},
  {arrival: '1', arrivalText: 'arrival Two', count: 10}
];
const participantDepartures: ParticipantDeparture[] = [
  {departure: '0', departureText: 'arrival One', count: 13},
  {departure: '1', departureText: 'arrival Two', count: 17},
  {departure: '1', departureText: 'arrival Two', count: 55}
];

const arrivalsAndDepartures: ParticipantArrivalsAndDepartures = {
  arrivals: participantArrivals,
  departures: participantDepartures
};



describe('Dashboard saga', () => {
  let sagaTester: SagaTester;
  let promise;
  beforeEach(() => {
    sagaTester = new SagaTester({INITIAL_STATE, reducers: dashboardReducer});
    sagaTester.start(rootSaga);
  });

  afterEach(() => {
    sagaTester.reset();
  });

  describe('getting hints', () => {
    beforeEach(() => {
      axios.get.mockImplementation(() => promise);
    });

    afterEach(() => {
      axios.get.mockReset();
    });

    it('dispatches GET_HINTS_SUCCEEDED', async () => {
      promise = Promise.resolve({data: hints});
      sagaTester.dispatch(getArrivalsAndDeparturesHints());
      await sagaTester.waitFor(DashboardEvents.GET_HINTS_SUCCEEDED);
    });
    it('dispatches GET_HINTS_FAILED, when error', async () => {
      promise = Promise.reject({message: 'error'});
      sagaTester.dispatch(getArrivalsAndDeparturesHints());
      await sagaTester.waitFor(DashboardEvents.GET_HINTS_FAILED);
    });
    it('dispatches GET_HINTS_FAILED, when null', async () => {
      promise = Promise.resolve({data: null});
      sagaTester.dispatch(getArrivalsAndDeparturesHints());
      await sagaTester.waitFor(DashboardEvents.GET_HINTS_FAILED);
    });
  });
  describe('getting arrivals and departures', () => {
    beforeEach(() => {
      axios.get.mockImplementation(() => promise);
    });

    afterEach(() => {
      axios.get.mockReset();
    });

    it('dispatches GET_ARRIVALS_ANS_DEPARTURES_SUCCEEDED', async () => {
      promise = Promise.resolve({data: arrivalsAndDepartures});
      sagaTester.dispatch(getArrivalsAndDepartures());
      await sagaTester.waitFor(DashboardEvents.GET_ARRIVALS_ANS_DEPARTURES_SUCCEEDED);
    });
    it('dispatches GET_ARRIVALS_ANS_DEPARTURES_FAILED, when error', async () => {
      promise = Promise.reject({message: 'error'});
      sagaTester.dispatch(getArrivalsAndDepartures());
      await sagaTester.waitFor(DashboardEvents.GET_ARRIVALS_ANS_DEPARTURES_FAILED);
    });
    it('dispatches GET_ARRIVALS_ANS_DEPARTURES_FAILED, when null', async () => {
      promise = Promise.resolve({data: null});
      sagaTester.dispatch(getArrivalsAndDepartures());
      await sagaTester.waitFor(DashboardEvents.GET_ARRIVALS_ANS_DEPARTURES_FAILED);
    });
  });

  describe('get total participants', () => {
    beforeEach(() => {
      axios.get.mockImplementation(() => promise);
    });

    afterEach(() => {
      axios.get.mockReset();
    });

    it('dispatches GET_TOTAL_SPONSORED_SUCCEEDED', async () => {
      promise = Promise.resolve({data: 1000});
      sagaTester.dispatch(getTotalAmountOfParticipants());
      await sagaTester.waitFor(DashboardEvents.GET_TOTAL_AMOUNT_OF_PARTICIPANTS_SUCCEEDED);
    });
  });

  describe('get daily fees', () => {
    const dailyFees = [
      {conferenceDayId: 1, conferenceDayNumber: 1, conferenceDayName: 'Day 1', amount: 10},
      {conferenceDayId: 2, conferenceDayNumber: 2, conferenceDayName: 'Day 2', amount: 15},
      {conferenceDayId: 3, conferenceDayNumber: 3, conferenceDayName: 'Day 3', amount: 20}
    ];
    beforeEach(() => {
      axios.get.mockImplementation(() => promise);
    });

    afterEach(() => {
      axios.get.mockReset();
    });

    it('dispatches GET_TOTAL_SPONSORED_SUCCEEDED', async () => {
      promise = Promise.resolve({data: dailyFees});
      sagaTester.dispatch(getDailyFeesPerDay());
      await sagaTester.waitFor(DashboardEvents.GET_DAILY_FEES_PER_DAY_SUCCEEDED);
    });
  });

});
