// @flow

import RoomSharingEvents from './roomSharingEvents';

export type SharingPerson = {
  personId: number,
  firstName: string,
  lastName: string,
  familyInfo: string,
  company: string,
  address1: string,
  address2: string,
  postal: string,
  province: string,
  city: string,
  country: string
}

export type RoomSharing = {
  invitee1: SharingPerson,
  invitee2: SharingPerson,
  roomType: string
}

export type RoomSharingState = {
  roomSharingData: RoomSharing[]
}

export const INITIAL_STATE: RoomSharingState = {
  roomSharingData: []
};

export const roomSharingReducer = (state: RoomSharingState = INITIAL_STATE, action: any) => {
  switch (action.type) {
    case RoomSharingEvents.GET_ROOM_SHARING_DATA_SUCCEEDED:
      return {...state, roomSharingData: action.roomSharingData};
    case RoomSharingEvents.GET_ROOM_SHARING_DATA_FAILED:
      return {...state, roomSharingData: []};
    default:
      return state;
  }
};

export default {roomSharing: roomSharingReducer};
