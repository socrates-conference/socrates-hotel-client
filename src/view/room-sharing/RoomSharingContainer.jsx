// @flow

import React, {Component} from 'react';
import * as PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {Action, ColumnDefinition, Table} from 'socrates-ui-components';
import {getRoomSharingData} from './roomSharingCommand';
import type {RoomSharing, SharingPerson} from './roomSharingReducer';

export type Props = {
  roomSharingData: RoomSharing[],
  getRoomSharingData: () => void,
}

class RoomSharingContainer extends Component<Props> {

  static propTypes = {
    getRoomSharingData: PropTypes.func.isRequired,
    roomSharingData: PropTypes.array.isRequired
  };

  _inviteeCellTemplate = (invitee: SharingPerson) =>
    <div>
      <div><strong>{invitee.firstName} {invitee.lastName}</strong></div>
      <div>{invitee.address1}</div>
      {invitee.address2 && <div>{invitee.address2}</div>}
      {invitee.province && <div>{invitee.province}</div>}
      <div>{invitee.postal}&nbsp;{invitee.city}</div>
      <div>{invitee.country}</div>
      {invitee.familyInfo && <br/>}
      {invitee.familyInfo && <div>{invitee.familyInfo}</div>}
    </div>;


  _tableActions: Array<Action> = [
    {
      execute: () => this.props.getRoomSharingData(),
      canExecute: () => true, buttonClass: 'btn-info',
      icon: 'sync', text: 'Refresh', visible: true
    }
  ];

  _columns: Array<ColumnDefinition> = [
    {header: 'Participant', field: 'invitee1', visible: true, cellTemplate: (name) => this._inviteeCellTemplate(name)},
    {header: 'Participant', field: 'invitee2', visible: true, cellTemplate: (name) => this._inviteeCellTemplate(name)},
    {header: 'Room type', field: 'roomType', visible: true}
  ];

  componentDidMount = () => {
    this.props.getRoomSharingData();
  };

  render = () => {
    const {roomSharingData} = this.props;
    return (
      <div className="container">
        <div className="row">
          <div className="col-sm-12">
            <div className="page-header"><h1>Room sharing list</h1></div>
          </div>
        </div>
        <Table
          columns={this._columns} data={roomSharingData}
          tableActions={this._tableActions}
        />
      </div>
    );
  };
}

const mapStateToProps = (state) => {
  return {...state.roomSharing};
};

const mapDispatchToProps = {
  getRoomSharingData
};

export default connect(mapStateToProps, mapDispatchToProps)(RoomSharingContainer);
