// @flow

import type {RoomSharing} from './roomSharingReducer';

export default class RoomSharingEvents {
  static get GET_ROOM_SHARING_DATA_SUCCEEDED() {
    return 'RoomOccupancyEvents/GET_ROOM_SHARING_DATA_SUCCEEDED';
  }

  static get GET_ROOM_SHARING_DATA_FAILED() {
    return 'RoomOccupancyEvents/GET_ROOM_SHARING_DATA_FAILED';
  }
}

export const getRoomSharingDataSucceeded =
  (roomSharingData: RoomSharing[]) =>
    ({type: RoomSharingEvents.GET_ROOM_SHARING_DATA_SUCCEEDED, roomSharingData});
export const getRoomSharingDataFailed = () =>
  ({type: RoomSharingEvents.GET_ROOM_SHARING_DATA_FAILED});
