//@flow

export default class RoomSharingCommand {
  static get GET_ROOM_SHARING_DATA(): string {
    return 'RoomOccupancyCommand/GET_ROOM_SHARING_DATA';
  }
}

export const getRoomSharingData = () => ({type: RoomSharingCommand.GET_ROOM_SHARING_DATA});
