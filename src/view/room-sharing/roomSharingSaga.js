// @flow
import * as api from '../../requests/api';
import {call, put} from 'redux-saga/effects';
import type {RoomSharing} from './roomSharingReducer';
import {getRoomSharingDataFailed, getRoomSharingDataSucceeded} from './roomSharingEvents';

export function* getRoomSharingDataSaga(): Iterable<any> {
  const roomSharingData: ?RoomSharing[] = yield call(api.readRoomSharingData);
  if (roomSharingData && roomSharingData.length > 0) {
    yield put(getRoomSharingDataSucceeded(roomSharingData));
  } else {
    yield put(getRoomSharingDataFailed());
  }
}
