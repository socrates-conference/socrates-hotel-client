import {getRoomSharingDataFailed, getRoomSharingDataSucceeded} from './roomSharingEvents';
import {INITIAL_STATE, roomSharingReducer} from './roomSharingReducer';

describe('Room sharing reducer', () => {
  it('stores sharing data when GET_ROOM_SHARING_DATA_SUCCEEDED', () => {
    const event = getRoomSharingDataSucceeded([
      {
        invitee1: {personId: 1, firstName: 'First1', lastName: 'Last1', familyInfo: 'familyInfo1'},
        invitee2: {personId: 2, firstName: 'First2', lastName: 'Last2', familyInfo: 'familyInfo2'},
        roomType: 'roomType'
      }
    ]);
    const result = roomSharingReducer(INITIAL_STATE, event);
    expect(result.roomSharingData).toBeDefined();
    expect(result.roomSharingData).toHaveLength(1);
  });
  it('stores empty array when GET_ROOM_SHARING_DATA_FAILED', () => {
    const event = getRoomSharingDataFailed();
    const result = roomSharingReducer(INITIAL_STATE, event);
    expect(result.roomSharingData).toBeDefined();
    expect(result.roomSharingData).toHaveLength(0);
  });
  it('does nothing if unknown event', () => {
    const event = {type: 'unknown'};
    const result = roomSharingReducer(INITIAL_STATE, event);
    expect(result.roomSharingData).toBeDefined();
    expect(result).toEqual(INITIAL_STATE);
  });
});
