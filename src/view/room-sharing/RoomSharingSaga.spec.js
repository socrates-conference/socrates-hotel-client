// @flow
import axios from 'axios';
import SagaTester from 'redux-saga-tester';
import rootSaga from '../../rootSaga';
import type {RoomSharing} from './roomSharingReducer';
import {INITIAL_STATE, roomSharingReducer} from './roomSharingReducer';
import {getRoomSharingData} from './roomSharingCommand';
import RoomSharingEvents from './roomSharingEvents';

jest.mock('axios');

const sharingData: Array<RoomSharing> = [
  {
    invitee1: {
      personId: 1, firstName: 'First1', lastName: 'Last1', familyInfo: 'familyInfo1',
      company: 'Company1', address1: 'Address1.1', address2: 'Address2.1', postal: 'Postal1',
      province: 'Province1', city: 'City1', country: 'Country1'
    },
    invitee2: {
      personId: 2, firstName: 'First2', lastName: 'Last2', familyInfo: 'familyInfo2',
      company: 'Company2', address1: 'Address1.2', address2: 'Address2.2', postal: 'Postal2',
      province: 'Province2', city: 'City2', country: 'Country2'
    },
    roomType: 'roomType'
  }
];

describe('on sharing saga', () => {
  let sagaTester: SagaTester;
  let promise;
  beforeEach(() => {
    sagaTester = new SagaTester({INITIAL_STATE, reducers: roomSharingReducer});
    sagaTester.start(rootSaga);
  });

  afterEach(() => {
    sagaTester.reset();
  });

  describe('getting room sharing data', () => {
    beforeEach(() => {
      axios.get.mockImplementation(() => promise);
    });

    afterEach(() => {
      axios.get.mockReset();
    });

    it('dispatches GET_ROOM_SHARING_DATA_SUCCEEDED', async () => {
      promise = Promise.resolve({data: sharingData});
      sagaTester.dispatch(getRoomSharingData());
      await sagaTester.waitFor(RoomSharingEvents.GET_ROOM_SHARING_DATA_SUCCEEDED);
    });
    it('dispatches GET_ROOM_SHARING_DATA_FAILED, when error', async () => {
      promise = Promise.reject({message: 'error'});
      sagaTester.dispatch(getRoomSharingData());
      await sagaTester.waitFor(RoomSharingEvents.GET_ROOM_SHARING_DATA_FAILED);
    });
    it('dispatches GET_ROOM_SHARING_DATA_FAILED, when null', async () => {
      promise = Promise.resolve({data: null});
      sagaTester.dispatch(getRoomSharingData());
      await sagaTester.waitFor(RoomSharingEvents.GET_ROOM_SHARING_DATA_FAILED);
    });
  });
});
