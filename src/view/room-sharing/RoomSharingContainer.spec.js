// @flow

import {shallow} from 'enzyme/build';
import React from 'react';
import StoreFactory, {HistoryFactory} from '../../store/store';
import reducers from '../../rootReducer';
import rootSaga from '../../rootSaga';
import RoomSharingContainer from './RoomSharingContainer';
import type {RoomSharing} from './roomSharingReducer';

const sharingData: Array<RoomSharing> = [
  {
    invitee1: {
      personId: 1, firstName: 'First1', lastName: 'Last1', familyInfo: 'familyInfo1',
      company: 'Company1', address1: 'Address1.1', address2: 'Address2.1', postal: 'Postal1',
      province: 'Province1', city: 'City1', country: 'Country1'
    },
    invitee2: {
      personId: 2, firstName: 'First2', lastName: 'Last2', familyInfo: 'familyInfo2',
      company: 'Company2', address1: 'Address1.2', address2: 'Address2.2', postal: 'Postal2',
      province: 'Province2', city: 'City2', country: 'Country2'
    },
    roomType: 'roomType'
  }
];

describe('(Component) RoomSharingContainer', () => {

  const history = HistoryFactory.createHistory();
  const store = StoreFactory.createStore(reducers, rootSaga, history);

  const wrapper = shallow(<RoomSharingContainer store={store}
    getRoomSharingData={() => {}}
    roomSharingData={sharingData}
  />);

  it('renders without exploding', () => {
    expect(wrapper).toHaveLength(1);
  });

  it('renders correctly', () => {
    expect(wrapper).toMatchSnapshot();
  });
});
