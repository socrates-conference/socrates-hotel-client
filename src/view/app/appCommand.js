//@flow

export default class AppCommand {
  static get GET_CONFERENCE(): string {
    return 'AppCommand/GET_CONFERENCE';
  }
}

export const getConference = () => ({type: AppCommand.GET_CONFERENCE});
