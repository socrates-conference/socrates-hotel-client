// @flow

import SagaTester from 'redux-saga-tester';
import rootSaga from '../../rootSaga';
import * as sinon from 'sinon';
import * as api from '../../requests/api';
import {getConference} from './appCommand';
import appReducer from './appReducer';

describe('rootSaga', () => {
  let sagaTester: SagaTester;
  describe('reacts to get conference data', () => {
    const INITIAL_STATE = {
      app: {
        conference: undefined
      }
    };
    const readConference = sinon.stub(api, 'readConference');

    beforeEach(() => {
      sagaTester = new SagaTester({INITIAL_STATE, reducers: appReducer});
      sagaTester.start(rootSaga);
    });

    afterEach(() => {
      readConference.reset();
      sagaTester.reset();
    });

    it('emitting success event when succeed', () => {
      readConference.returns({id: 4711});
      sagaTester.dispatch(getConference());
      expect(readConference.getCalls().length).toBe(1);
      const conference = sagaTester.getState().app.conference;
      expect(conference).toBeDefined();
      expect(conference).toEqual({id: 4711});
    });
  });
});
