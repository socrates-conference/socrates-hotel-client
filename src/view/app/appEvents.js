// @flow

import type {Conference} from './appReducer';

export default class AppEvents {
  static get GET_CONFERENCE_SUCCEEDED() { return 'AppEvents/GET_CONFERENCE_SUCCEEDED'; }
  static get GET_CONFERENCE_FAILED() { return 'AppEvents/GET_CONFERENCE_FAILED'; }
}

export const getConferenceSucceeded =
  (conference: Conference) => ({type: AppEvents.GET_CONFERENCE_SUCCEEDED, conference});
export const getConferenceFailed = (error: any) => ({type: AppEvents.GET_CONFERENCE_FAILED, error});
