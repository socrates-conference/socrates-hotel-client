import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import rootSaga from '../../rootSaga';
import StoreFactory, {HistoryFactory} from '../../store/store';
import reducers from '../../rootReducer';

it('renders without crashing', () => {
  const history = HistoryFactory.createHistory();
  const store = StoreFactory.createStore(reducers, rootSaga, history);

  const div = document.createElement('div');
  ReactDOM.render(<App store={store} history={history}/>, div);
});
