// @flow

import React, {Component} from 'react';
import * as PropTypes from 'prop-types';
import './App.css';
import {
  connect,
  Provider
} from 'react-redux';
import {
  Redirect,
  Route,
  Router,
  Switch
} from 'react-router-dom';
import ScrollToTop from '../common/ScrollToTop';
import Navigation from '../common/navigation/Navigation';
import Home from '../dashboard/Home';
import Login from '../authentication/Login';
import Sponsors from '../sponsors-list/Sponsors';
// noinspection ES6CheckImport
import {Imprint, PrivacyPolicy} from 'socrates-ui-components';
import RoomOccupancyContainer from '../room-occupancy/RoomOccupancyContainer';
import RoomSharingContainer from '../room-sharing/RoomSharingContainer';
import DataLogContainer from '../data-changes-log/DataLogContainer';
import PrivateRoute from '../PrivateRoute';

export type Props = {
  store: any,
  history: any,
}

export class App extends Component<Props> {
  static propTypes = {
    history: PropTypes.any,
    store: PropTypes.any
  };

  static defaultProps = {};

  render() {
    return (
      <Provider store={this.props.store}>
        <Router history={this.props.history}>
          <ScrollToTop>
            <Switch>
              <Redirect exact from="/" to="/home"/>
              <PrivateRoute path="/home">
                <div>
                  <Navigation/>
                  <Home/>
                </div>
              </PrivateRoute>
              <PrivateRoute path="/sponsors">
                <div>
                  <Navigation/>
                  <Sponsors/>
                </div>
              </PrivateRoute>
              <PrivateRoute path="/room-occupancy">
                <div>
                  <Navigation/>
                  <RoomOccupancyContainer/>
                </div>
              </PrivateRoute>
              <PrivateRoute path="/room-sharing">
                <div>
                  <Navigation/>
                  <RoomSharingContainer/>
                </div>
              </PrivateRoute>
              <PrivateRoute path="/data-log">
                <div>
                  <Navigation/>
                  <DataLogContainer/>
                </div>
              </PrivateRoute>
              <Route path="/imprint">
                <div>
                  <Navigation/>
                  <Imprint/>
                </div>
              </Route>
              <Route path="/privacy-policy">
                <div>
                  <Navigation/>
                  <PrivacyPolicy/>
                </div>
              </Route>
              <Route path="/login">
                <div>
                  <Navigation/>
                  <Login/>
                </div>
              </Route>
            </Switch>
          </ScrollToTop>
        </Router>
      </Provider>
    );
  }
}

const mapStateToProps = (state) => {
  return {...state};
};

export default connect(mapStateToProps)(App);
