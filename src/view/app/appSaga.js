// @flow
import {call, put} from 'redux-saga/effects';
import {getConferenceFailed, getConferenceSucceeded} from './appEvents';
import * as api from '../../requests/api';
import type {Conference} from './appReducer';

export function* conferenceGetData(): Iterable<any> {
  try {
    const conference: ?Conference = yield call(api.readConference);
    if(conference) {
      yield put(getConferenceSucceeded(conference));
    } else {
      yield put(getConferenceFailed(new Error('No conference data available.')));
    }
  } catch(error) {
    yield put(getConferenceFailed(error));
  }
}
