// @flow
import RoutingCommand from './view/routingCommand';
import AuthenticationCommand from './view/authentication/authenticationCommand';
import AppCommand from './view/app/appCommand';
import {loginSaga, logoutSaga} from './view/authentication/authorizationSaga';
import {routeToSaga} from './view/routeToSaga';
import {conferenceGetData} from './view/app/appSaga';
import DashboardCommand from './view/dashboard/dashboardCommand';
import {getArrivalsAndDeparturesSaga, getHintsSaga,
  getTotalAmountOfParticipantsSaga, getDailyFeesSaga} from './view/dashboard/dashboardSaga';
import {all, takeEvery} from 'redux-saga/effects';
import SponsorsCommand from './view/sponsors-list/sponsorsCommand';
import {getSponsorsSaga, getTotalSponsoredSaga} from './view/sponsors-list/sponsorsSaga';
import RoomOccupancyCommand from './view/room-occupancy/roomOccupancyCommand';
import {
  changeRoomTypeSaga, downloadParticipantsByRoomTypeFileSaga,
  getParticipantsByRoomTypeSaga,
  getRoomTypesSaga
} from './view/room-occupancy/roomOccupancySaga';
import RoomSharingCommand from './view/room-sharing/roomSharingCommand';
import {getRoomSharingDataSaga} from './view/room-sharing/roomSharingSaga';
import DataChangesCommand from './view/data-changes-log/dataChangesCommand';
import {getChangesSaga} from './view/data-changes-log/dataChangesSaga';

function* rootSaga(): any {
  yield all([
    takeEvery(AppCommand.GET_CONFERENCE, conferenceGetData),
    takeEvery(RoutingCommand.ROUTE_TO, routeToSaga),
    takeEvery(AuthenticationCommand.LOGIN, loginSaga),
    takeEvery(AuthenticationCommand.LOGOUT, logoutSaga),
    takeEvery(DashboardCommand.GET_ARRIVALS_AND_DEPARTURES_HINTS, getHintsSaga),
    takeEvery(DashboardCommand.GET_ARRIVALS_AND_DEPARTURES, getArrivalsAndDeparturesSaga),
    takeEvery(DashboardCommand.GET_TOTAL_AMOUNT_OF_PARTICIPANTS, getTotalAmountOfParticipantsSaga),
    takeEvery(SponsorsCommand.GET_SPONSORS, getSponsorsSaga),
    takeEvery(SponsorsCommand.GET_TOTAL_SPONSORED, getTotalSponsoredSaga),
    takeEvery(DashboardCommand.GET_DAILY_FEES_PER_DAY, getDailyFeesSaga),
    takeEvery(RoomOccupancyCommand.GET_ROOM_TYPES, getRoomTypesSaga),
    takeEvery(RoomOccupancyCommand.GET_PARTICIPANTS_BY_ROOM_TYPE, getParticipantsByRoomTypeSaga),
    takeEvery(RoomOccupancyCommand.CHANGE_ROOM_TYPE, changeRoomTypeSaga),
    takeEvery(RoomSharingCommand.GET_ROOM_SHARING_DATA, getRoomSharingDataSaga),
    takeEvery(RoomOccupancyCommand.DOWNLOAD_BY_ROOM_TYPE, downloadParticipantsByRoomTypeFileSaga),
    takeEvery(DataChangesCommand.GET_CHANGES, getChangesSaga)
  ]);
}


export default rootSaga;
