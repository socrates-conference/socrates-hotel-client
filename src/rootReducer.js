// @flow

import authenticationReducer from './view/authentication/authenticationReducer';
import appReducer from './view/app/appReducer';
import hotelReducer from './view/dashboard/dashboardReducer';
import sponsorsReducer from './view/sponsors-list/sponsorsReducer';
import roomOccupancyReducer from './view/room-occupancy/roomOccupancyReducer';
import roomSharingReducer from './view/room-sharing/roomSharingReducer';
import dataChangesReducer from './view/data-changes-log/dataChangesReducer';

export default Object.assign({}, appReducer, authenticationReducer, hotelReducer,
  sponsorsReducer, roomOccupancyReducer, roomSharingReducer, dataChangesReducer);
