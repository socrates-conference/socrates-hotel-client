// @flow
export type Config = {
  environment: string,
  jwtSecret: string,
  serverBackend: string,
  hotelBackend: string
}

