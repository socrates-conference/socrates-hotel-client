// @flow

import config from './config';

describe('config', () => {
  it('contains server object', () => {
    expect(config.environment).toBeDefined();
  });
  it('defines server port', () => {
    expect(config.hotelBackend).toBeDefined();
  });
  it('contains environment definition', () => {
    expect(config.serverBackend).toBeDefined();
  });
  it('contains database object', () => {
    expect(config.jwtSecret).toBeDefined();
  });
});
