// @flow

import type {Config} from './ConfigType';

const config: Config = {
  environment: 'dev',
  jwtSecret: '$lsRTf!gksTRcDWs', // jwt key to work locally.
  serverBackend: '/server/api/v1',
  hotelBackend: '/server/api/v1'
};

export default config;
