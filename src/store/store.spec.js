// @flow

import rootSaga from '../rootSaga';
import StoreFactory, {HistoryFactory} from './store';
import rootReducer from '../rootReducer';

describe('store', () => {
  let store;
  it('can be created', () => {
    store = StoreFactory.createStore(rootReducer, rootSaga, HistoryFactory.createTestHistory());
    expect(store).toBeDefined();
  });
  it('can create a test store', () => {
    store = StoreFactory.createTestStore(rootReducer, rootSaga, HistoryFactory.createTestHistory());
    expect(store).toBeDefined();
  });
});
