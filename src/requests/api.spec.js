// @flow

import axios from 'axios/index';
import {
  login,
  readConference,
  readHints,
  readSponsors,
  readTotalSponsored,
  readArrivalsAndDepartures,
  readAmountOfParticipants,
  readDailyFeesPerDay,
  readRoomTypes,
  readParticipantsByRoomType,
  readRoomSharingData
} from './api';
import type {
  ParticipantArrival,
  ParticipantArrivalsAndDepartures,
  ParticipantDeparture
} from '../view/dashboard/dashboardReducer';
import type {RoomSharing} from '../view/room-sharing/roomSharingReducer';


jest.mock('axios');

describe('api', () => {
  describe('login', () => {
    let promise;
    beforeEach(() => {
      axios.post.mockImplementation(() => {
        return promise;
      });
    });
    afterEach(() => {
      axios.post.mockReset();
    });
    it('returns the token string, as received from the server on calling login', (done) => {
      promise = Promise.resolve({data: {token: 'TheToken'}});
      login('token').then((result) => {
        expect(result).toEqual('TheToken');
        done();
      });
    });
    it('login returns empty string if server request fails', (done) => {
      promise = Promise.reject(new Error('error'));
      login('token').catch((result) => {
        expect(result.message).toBe('error');
        done();
      });
    });
  });
  describe('reads app data', () => {
    let promise;
    beforeEach(() => {
      axios.get.mockImplementation(() => {
        return promise;
      });
    });
    afterEach(() => {
      axios.get.mockReset();
    });
    it('returns conference data', (done: (?string)=> void) => {
      promise = Promise.resolve({data: {name: 'SoCraTes'}});
      readConference().then((result) => {
        expect(result.name).toEqual('SoCraTes');
        done();
      });
    });
    it('throws on error', (done: (?string)=> void) => {
      promise = Promise.reject({message: 'Test error'});
      readConference().then(() => {
        done('expected exception!');
      }).catch(() => {
        done();
      });
    });
    it('throws if conference data is null / undefined', (done: (?string)=> void) => {
      promise = Promise.resolve({data: null});
      readConference().then(() => {
        done('expected exception!');
      }).catch(() => {
        done();
      });
    });
    it('throws if conference data miss name', (done: (?string)=> void) => {
      promise = Promise.resolve({});
      readConference().then(() => {
        done('expected exception!');
      }).catch(() => {
        done();
      });
    });
  });
  describe('reads hints', () => {
    let promise;
    beforeEach(() => {
      axios.get.mockImplementation(() => {
        return promise;
      });
    });
    afterEach(() => {
      axios.get.mockReset();
    });
    it('returns hints data', (done: (?string)=> void) => {
      promise = Promise.resolve({
        data: {
          arrivals: [{id: 'one', name: 'Arrival one', hint: 'Hint arrival one'}],
          departures: [{id: 'one', name: 'Departure one', hint: 'Hint departure one'}]
        }
      });
      readHints().then((result) => {
        expect(result.arrivals).toHaveLength(1);
        expect(result.departures).toHaveLength(1);
        done();
      });
    });
    it('throws on error', (done: (?string)=> void) => {
      promise = Promise.reject({message: 'Test error'});
      readHints().then(() => {
        done('expected exception!');
      }).catch(() => {
        done();
      });
    });
    it('throws if hints is null / undefined', (done: (?string)=> void) => {
      promise = Promise.resolve({data: null});
      readHints().then(() => {
        done('expected exception!');
      }).catch(() => {
        done();
      });
    });
  });
  describe('reads sponsors', () => {
    let promise;
    beforeEach(() => {
      axios.get.mockImplementation(() => {
        return promise;
      });
    });
    afterEach(() => {
      axios.get.mockReset();
    });
    it('returns sponsors', (done: (?string)=> void) => {
      promise = Promise.resolve({
        data: [{}]
      });
      readSponsors().then((result) => {
        expect(result).toHaveLength(1);
        done();
      });
    });
    it('throws on error', (done: (?string)=> void) => {
      promise = Promise.reject({message: 'Test error'});
      readSponsors().then(() => {
        done('expected exception!');
      }).catch(() => {
        done();
      });
    });
    it('throws if hints is null / undefined', (done: (?string)=> void) => {
      promise = Promise.resolve({data: null});
      readSponsors().then(() => {
        done('expected exception!');
      }).catch(() => {
        done();
      });
    });
  });
  describe('reads total sponsored', () => {
    let promise;
    beforeEach(() => {
      axios.get.mockImplementation(() => {
        return promise;
      });
    });
    afterEach(() => {
      axios.get.mockReset();
    });
    it('returns conference data', (done: (?string)=> void) => {
      promise = Promise.resolve({
        data: 100
      });
      readTotalSponsored().then((result) => {
        expect(result).toBe(100);
        done();
      });
    });
    it('throws on error', (done: (?string)=> void) => {
      promise = Promise.reject({message: 'Test error'});
      readTotalSponsored().then((result) => {
        expect(result).toBe(0);
        done();
      });
    });
    it('throws if hints is null / undefined', (done: (?string)=> void) => {
      promise = Promise.resolve({data: null});
      readTotalSponsored().then((result) => {
        expect(result).toBe(0);
        done();
      });
    });
  });
  describe('reads arrivals and departures', () => {
    const participantArrivals: ParticipantArrival[] = [
      {arrival: '0', arrivalText: 'arrival One', count: 20},
      {arrival: '1', arrivalText: 'arrival Two', count: 10}
    ];
    const participantDepartures: ParticipantDeparture[] = [
      {departure: '0', departureText: 'arrival One', count: 13},
      {departure: '1', departureText: 'arrival Two', count: 17},
      {departure: '1', departureText: 'arrival Two', count: 55}
    ];

    const arrivalsAndDepartures: ParticipantArrivalsAndDepartures = {
      arrivals: participantArrivals,
      departures: participantDepartures
    };

    let promise;

    beforeEach(() => {
      axios.get.mockImplementation(() => {
        return promise;
      });
    });
    afterEach(() => {
      axios.get.mockReset();
    });
    it('returns arrivals and departures summary data', (done: (?string)=> void) => {
      promise = Promise.resolve({data: arrivalsAndDepartures});
      readArrivalsAndDepartures().then((result) => {
        expect(result).toEqual(arrivalsAndDepartures);
        done();
      });
    });
    it('throws on error', (done: (?string)=> void) => {
      promise = Promise.reject({message: 'Test error'});
      readArrivalsAndDepartures().then(() => {
        done('expected exception!');
      }).catch(() => {
        done();
      });
    });
    it('throws if hints is null / undefined', (done: (?string)=> void) => {
      promise = Promise.resolve({data: null});
      readArrivalsAndDepartures().then(() => {
        done('expected exception!');
      }).catch(() => {
        done();
      });
    });
  });
  describe('reads total participants', () => {
    let promise;
    beforeEach(() => {
      axios.get.mockImplementation(() => {
        return promise;
      });
    });
    afterEach(() => {
      axios.get.mockReset();
    });
    it('returns conference data', (done: (?string)=> void) => {
      promise = Promise.resolve({
        data: 200
      });
      readAmountOfParticipants().then((result) => {
        expect(result).toBe(200);
        done();
      });
    });
    it('throws on error', (done: (?string)=> void) => {
      promise = Promise.reject({message: 'Test error'});
      readAmountOfParticipants().then((result) => {
        expect(result).toBe(0);
        done();
      });
    });
    it('throws if hints is null / undefined', (done: (?string)=> void) => {
      promise = Promise.resolve({data: null});
      readAmountOfParticipants().then((result) => {
        expect(result).toBe(0);
        done();
      });
    });
  });
  describe('reads sponsors', () => {
    let promise;
    beforeEach(() => {
      axios.get.mockImplementation(() => {
        return promise;
      });
    });
    afterEach(() => {
      axios.get.mockReset();
    });
    it('returns sponsors', (done: (?string)=> void) => {
      promise = Promise.resolve({
        data: [{}]
      });
      readSponsors().then((result) => {
        expect(result).toHaveLength(1);
        done();
      });
    });
    it('throws on error', (done: (?string)=> void) => {
      promise = Promise.reject({message: 'Test error'});
      readSponsors().then(() => {
        done('expected exception!');
      }).catch(() => {
        done();
      });
    });
    it('throws if hints is null / undefined', (done: (?string)=> void) => {
      promise = Promise.resolve({data: null});
      readSponsors().then(() => {
        done('expected exception!');
      }).catch(() => {
        done();
      });
    });
  });
  describe('reads total sponsored', () => {
    let promise;
    beforeEach(() => {
      axios.get.mockImplementation(() => {
        return promise;
      });
    });
    afterEach(() => {
      axios.get.mockReset();
    });
    it('returns conference data', (done: (?string)=> void) => {
      promise = Promise.resolve({
        data: 100
      });
      readTotalSponsored().then((result) => {
        expect(result).toBe(100);
        done();
      });
    });
    it('return zero on error', (done: (?string)=> void) => {
      promise = Promise.reject({message: 'Test error'});
      readTotalSponsored().then((result) => {
        expect(result).toBe(0);
        done();
      });
    });
    it('returns zero if hints is null / undefined', (done: (?string)=> void) => {
      promise = Promise.resolve({data: null});
      readTotalSponsored().then((result) => {
        expect(result).toBe(0);
        done();
      });
    });
  });
  describe('reads amount of daily fees per day', () => {
    let promise;
    beforeEach(() => {
      axios.get.mockImplementation(() => {
        return promise;
      });
    });
    afterEach(() => {
      axios.get.mockReset();
    });
    it('returns daily fees', (done: (?string)=> void) => {
      const dailyFees = [
        {conferenceDayId: 1, conferenceDayNumber: 1, conferenceDayName: 'Day 1', amount: 10},
        {conferenceDayId: 2, conferenceDayNumber: 2, conferenceDayName: 'Day 2', amount: 15},
        {conferenceDayId: 3, conferenceDayNumber: 3, conferenceDayName: 'Day 3', amount: 20}
      ];
      promise = Promise.resolve({
        data: dailyFees
      });
      readDailyFeesPerDay().then((result) => {
        expect(result).toEqual(dailyFees);
        done();
      });
    });
    it('return empty array on error', (done: (?string)=> void) => {
      promise = Promise.reject({message: 'Test error'});
      readDailyFeesPerDay().then((result) => {
        expect(result).toEqual([]);
        done();
      });
    });
    it('return empty array if null / undefined', (done: (?string)=> void) => {
      promise = Promise.resolve({data: null});
      readDailyFeesPerDay().then((result) => {
        expect(result).toEqual([]);
        done();
      });
    });
  });
  describe('reads room types', () => {
    let promise;
    beforeEach(() => {
      axios.get.mockImplementation(() => {
        return promise;
      });
    });
    afterEach(() => {
      axios.get.mockReset();
    });
    it('returns room types', (done: (?string)=> void) => {
      const roomTypes = [
        {id: 1, conferenceEditionId: 1, display: 'Type 1'},
        {id: 2, conferenceEditionId: 1, display: 'Type 2'},
        {id: 3, conferenceEditionId: 1, display: 'Type 3'},
        {id: 4, conferenceEditionId: 1, display: 'Type 4'}
      ];
      promise = Promise.resolve({
        data: roomTypes
      });
      readRoomTypes().then((result) => {
        expect(result).toEqual(roomTypes);
        done();
      });
    });
    it('return empty array on error', (done: (?string)=> void) => {
      promise = Promise.reject({message: 'Test error'});
      readRoomTypes().then((result) => {
        expect(result).toEqual([]);
        done();
      });
    });
    it('return empty array if null / undefined', (done: (?string)=> void) => {
      promise = Promise.resolve({data: null});
      readRoomTypes().then((result) => {
        expect(result).toEqual([]);
        done();
      });
    });
  });
  describe('reads participants by room types', () => {
    let promise;
    beforeEach(() => {
      axios.get.mockImplementation(() => {
        return promise;
      });
    });
    afterEach(() => {
      axios.get.mockReset();
    });
    it('returns participants', (done: (?string)=> void) => {
      const participants = [
        {id: 1, personId: 1, name: 'name'}
      ];
      promise = Promise.resolve({
        data: participants
      });
      readParticipantsByRoomType('single').then((result) => {
        expect(result).toEqual(participants);
        done();
      });
    });
    it('return empty array on error', (done: (?string)=> void) => {
      promise = Promise.reject({message: 'Test error'});
      readParticipantsByRoomType('single').then((result) => {
        expect(result).toEqual([]);
        done();
      });
    });
    it('return empty array if null / undefined', (done: (?string)=> void) => {
      promise = Promise.resolve({data: null});
      readParticipantsByRoomType('single').then((result) => {
        expect(result).toEqual([]);
        done();
      });
    });
  });
  describe('reads room sharing data', () => {
    let promise;
    beforeEach(() => {
      axios.get.mockImplementation(() => {
        return promise;
      });
    });
    afterEach(() => {
      axios.get.mockReset();
    });
    it('returns room sharing data', (done: (?string)=> void) => {
      const sharingData: Array<RoomSharing> = [
        {
          invitee1: {
            personId: 1, firstName: 'First1', lastName: 'Last1', familyInfo: 'familyInfo1',
            company: 'Company1', address1: 'Address1.1', address2: 'Address2.1', postal: 'Postal1',
            province: 'Province1', city: 'City1', country: 'Country1'
          },
          invitee2: {
            personId: 2, firstName: 'First2', lastName: 'Last2', familyInfo: 'familyInfo2',
            company: 'Company2', address1: 'Address1.2', address2: 'Address2.2', postal: 'Postal2',
            province: 'Province2', city: 'City2', country: 'Country2'
          },
          roomType: 'roomType'
        }
      ];
      promise = Promise.resolve({
        data: sharingData
      });
      readRoomSharingData().then((result) => {
        expect(result).toEqual(sharingData);
        done();
      });
    });
    it('return empty array on error', (done: (?string)=> void) => {
      promise = Promise.reject({message: 'Test error'});
      readRoomSharingData().then((result) => {
        expect(result).toEqual([]);
        done();
      });
    });
    it('return empty array if null / undefined', (done: (?string)=> void) => {
      promise = Promise.resolve({data: null});
      readRoomSharingData().then((result) => {
        expect(result).toEqual([]);
        done();
      });
    });
  });
});

