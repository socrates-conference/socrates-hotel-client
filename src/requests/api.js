// @flow

import axios from 'axios/index';
import config from '../config/config';
import type {Conference} from '../view/app/appReducer';
import moment from 'moment/moment';
import type {Sponsor} from '../view/sponsors-list/sponsorsReducer';
import type {DailyFee, Hints, ParticipantArrivalsAndDepartures} from '../view/dashboard/dashboardReducer';

export const login = (requestToken: string): Promise<string> => {
  return axios.post(`${config.hotelBackend}/login`, {requestToken})
    .then(response => response.data.token);
};


export const readConference = async (): Promise<Conference> => {
  try {
    const {data} = await axios.get(config.hotelBackend + '/management/conference');
    if (data && data.name) {
      return {
        name: data.name,
        nameLong: data.nameLong,
        year: data.year,
        startDate: moment(data.startDate),
        endDate: moment(data.endDate),
        startApplications: moment(data.startApplications),
        endApplications: moment(data.endApplications),
        lotteryDay: moment(data.lotteryDay),
        dinnerPrice: data.dinnerPrice,
        flatThursday: data.flatThursday,
        flatFridaySaturday: data.flatFridaySaturday,
        flatSunday: data.flatSunday,
        roomTypes: data.roomTypes,
        lengthsOfStay: data.lengthsOfStay
      };
    } else {
      throw new Error('No Conference Data available');
    }
  } catch (e) {
    throw new Error(e.message);
  }
};

export const readHints = async (): Promise<Hints> => {
  try {
    const {data} = await axios.get(config.hotelBackend + '/hotel/arrival-departure-hints');
    if (data) {
      return data;
    } else {
      throw new Error('No Hints Data available');
    }
  } catch (e) {
    throw new Error(e.message);
  }
};

export const readAmountOfParticipants = async (): Promise<number> => {
  try {
    const {data} = await axios.get(config.hotelBackend + '/hotel/total-participants');
    if (data) {
      return data;
    } else {
      throw new Error('No participants data available');
    }
  } catch (e) {
    return 0;
  }
};

export const readArrivalsAndDepartures = async (): Promise<ParticipantArrivalsAndDepartures> => {
  try {
    const {data} = await axios.get(config.hotelBackend + '/hotel/arrivals-departures');
    if (data) {
      return data;
    } else {
      throw new Error('No Data available');
    }
  } catch (e) {
    throw new Error(e.message);
  }
};

export const readSponsors = async (): Promise<Sponsor[]> => {
  try {
    const {data} = await axios.get(config.hotelBackend + '/hotel/sponsors-list');
    if (data) {
      return data;
    } else {
      throw new Error('No sponsors Data available');
    }
  } catch (e) {
    throw new Error(e.message);
  }
};

export const readTotalSponsored = async (): Promise<number> => {
  try {
    const {data} = await axios.get(config.hotelBackend + '/hotel/total-sponsored');
    return data ? data : 0;
  } catch (e) {
    return 0;
  }
};

export const readDailyFeesPerDay = async (): Promise<DailyFee[]> => {
  try {
    const {data} = await axios.get(config.hotelBackend + '/hotel/fees-per-day');
    return data ? data : [];
  } catch (e) {
    return [];
  }
};

export const readRoomTypes = async (): Promise<[]> => {
  try {
    const {data} = await axios.get(config.hotelBackend + '/hotel/room-types');
    return data ? data : [];
  } catch (e) {
    return [];
  }
};

export const readParticipantsByRoomType = async (roomType: string): Promise<[]> => {
  try {
    const {data} = await axios.get(`${config.hotelBackend}/hotel/accommodated-in/${roomType}`);
    return data ? data : [];
  } catch (e) {
    return [];
  }
};

export const downloadFile = async (roomType: string) => {
  try {
    axios({
      url: `${config.hotelBackend}/hotel/download/${roomType}`,
      method: 'GET',
      responseType: 'blob' // important
    }).then((response) => {
      if( window && document && document.body) {
        const url = window.URL.createObjectURL(new Blob([response.data]));
        const link = document.createElement('a');
        link.href = url;
        link.setAttribute('download', `participants-${roomType}.csv`);
        // $FlowFixMe
        document.body.appendChild(link);
        link.click();
      }
    });
  } catch (e) {
    return undefined;
  }
};

export const readRoomSharingData = async (): Promise<[]> => {
  try {
    const {data} = await axios.get(`${config.hotelBackend}/hotel/room-sharing`);
    return data ? data : [];
  } catch (e) {
    return [];
  }
};

export const readChanges = async (dateFrom: string): Promise<[]> => {
  try {
    const {data} = await axios.get(`${config.hotelBackend}/hotel/changes?from=${dateFrom}`);
    return data ? data : [];
  } catch (e) {
    return [];
  }
};

