// @flow
/* eslint-disable */
import type {Config} from '../../src/config/ConfigType';

const config: Config = {
  environment: 'prod',
  jwtSecret: '%jwtsecret%',
  serverBackend: '/server/api/v1',
  hotelBackend: 'http://18.197.143.144/server/api/v1'
};
export default config;
