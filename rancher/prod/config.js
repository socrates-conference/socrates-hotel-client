// @flow
/* eslint-disable */
import type {Config} from '../../src/config/ConfigType';

const config: Config = {
  environment: 'prod',
  jwtSecret: '%jwtsecret%',
  serverBackend: '/server/api/v1',
  hotelBackend: 'https://www.socrates-conference.de/server/api/v1'
};
export default config;
